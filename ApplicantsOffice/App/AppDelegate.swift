//
//  AppDelegate.swift
//  applicant'sOffice
//
//  Created by Евгений Балашов on 21/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit
import UserNotifications


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var launch: Bool = false
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        SPFirebase.configure()
        SPFabric.configure()
        
        if (!Preferences.isNeedAuth()){
            Api.getSystemStatistic()
            Api.checkForUpdate()
            
            redirectToIncidents()
        }else{
            if (Preferences.getInstans().getLicense() == ""){
                redirectTiLicence()
            }
        }
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge,.alert,.sound]){ (sucess,error) in
            if error != nil{
            }else{
            }
        }
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }
    
    // MARK: UISceneSession Lifecycle
    
    //    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
    //        // Called when a new scene session is being created.
    //        // Use this method to select a configuration to create the new scene with.
    //        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    //    }
    //
    //    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    //        // Called when the user discards a scene session.
    //        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    //        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    //    }
    
    func redirectToIncidents(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let menuTabController = storyboard.instantiateViewController(withIdentifier: "tabBar") as! TabBarController
        menuTabController.selectedIndex = 2;
        window?.rootViewController = menuTabController
    }
    
    func redirectTiLicence(){
        let licenseStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let licenseController = licenseStoryboard.instantiateViewController(withIdentifier: "license") as! LicenseAgreementController
        window?.rootViewController = licenseController
    }
    
}

