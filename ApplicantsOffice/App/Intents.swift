//
//  Intents.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 01/11/2019.
//  Copyright © 2019 example. All rights reserved.
//

import Intents
import UIKit

enum Intents: String {
    
    case newIncident = "NewIncident"
    case showIncidents = "ShowIncidents"
    
    var id: String {
        return "com.example.applicant-sOffice.intents.\(self.rawValue.lowercased())"
    }
    
    func setActive(title: String, phrase: String?, controller: UIViewController) {
        
        let activity = NSUserActivity(activityType: self.id)
        activity.title = title
        
        if #available(iOS 12.0, *) {
            activity.isEligibleForPrediction = true
            activity.suggestedInvocationPhrase = phrase
        }
        
        activity.isEligibleForSearch = true
        activity.isEligibleForHandoff = true
        
        controller.userActivity = activity
        activity.becomeCurrent()
    }
}
