//
//  Equipment.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 23/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import Foundation
import SwiftyJSON

class Equipment: NSObject {
    
    var original_id:Int
    var equipment_title:String
    var equipment_uin:String
    var equipment_markplace:String
    
    
    init?(item: [String: JSON]) {
        guard let original_id = item["original_id"]?.int, let equipment_title = item["equipment_title"]?.string, let equipment_uin = item["equipment_uin"]?.string,
            let equipment_markplace = item["equipment_markplace"]?.string else { return nil }
        
        self.original_id = original_id
        self.equipment_title = equipment_title
        self.equipment_uin = equipment_uin
        self.equipment_markplace = equipment_markplace
    }
}
