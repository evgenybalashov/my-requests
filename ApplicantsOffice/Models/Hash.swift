//
//  Hash.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 25/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

struct Hash {
    var incident_id: Int
    var crc32_hash: Int
    
    init?(item: [String: JSON]) {
        guard let incident_id = item["incident_id"]?.int, let crc32_hash = item["crc32_hash"]?.int  else { return nil }
        
        self.incident_id = incident_id
        self.crc32_hash = crc32_hash
    }
}
