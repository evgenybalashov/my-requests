//
//  File.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 23/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON

struct Incident {
    
    var incident_id: Int
    var crc32_hash: Int
    var status_title: String
    var timestamp_create: Int
    var equipment_title: String
    var equipment_uin: String?
    var equipment_markplace: String?
    var in_work = false
    var timestamp_close: Int?
    var problems: String?
    var additional_info: String?
    var is_need_rating: Bool = false
    var rating: Int
    var rating_comment: String
    var photo: String
    var isDeleted = false
    
    init(incident_id: Int, crc32_hash: Int, status_title: String, timestamp_create: Int, equipment_title: String, equipment_uin: String?, equipment_markplace: String?, in_work: Bool, timestamp_close: Int?, problems: String?, additional_info: String?, is_need_rating: Bool, rating: Int?, rating_comment: String?, photo: String?) {
        self.incident_id = incident_id
        self.crc32_hash = crc32_hash
        self.status_title = status_title
        self.timestamp_create = timestamp_create
        self.equipment_title = equipment_title
        self.equipment_uin = equipment_uin
        self.equipment_markplace = equipment_markplace
        self.in_work = in_work
        self.timestamp_close = timestamp_close ?? 0
        self.problems = problems
        self.additional_info = additional_info
        self.is_need_rating = is_need_rating
        self.rating = rating ?? 0
        self.rating_comment = rating_comment ?? ""
        self.photo = photo ?? ""
    }
    
    var complete: Bool {
        return self.timestamp_close != nil
    }
}


class IncidentStorage: Object {
    
    @objc dynamic var incident_id: Int = 0
    @objc dynamic var crc32_hash: Int = 0
    @objc dynamic var status_title: String = ""
    @objc dynamic var timestamp_create: Int = 0
    @objc dynamic var equipment_title: String = ""
    @objc dynamic var equipment_uin: String? = ""
    @objc dynamic var equipment_markplace: String? = ""
    @objc dynamic var in_work: Bool = false
    @objc dynamic var timestamp_close: Int = 0
    @objc dynamic var problems: String? = ""
    @objc dynamic var additional_info: String? = ""
    @objc dynamic var is_need_rating: Bool = false
    @objc dynamic var rating: Int = 0
    @objc dynamic var rating_comment: String? = ""
    @objc dynamic var photo: String? = ""
    @objc dynamic var isDeleted = false
    
    
    //    override class func primaryKey() -> String? {
    //        return "id"
    //    }
}
