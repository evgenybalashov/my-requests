//
//  Notifi.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 15/11/2019.
//  Copyright © 2019 example. All rights reserved.
//


import Foundation
import SwiftyJSON
import Alamofire

struct Notifi {
    
    var notifi_id: Int
    var title: String
    var body: String
    
    init?(item: [String: JSON]) {
        guard let notifi_id = item["notify_id"]?.int, let title = item["title"]?.string, let body = item["body"]?.string  else { return nil }
        
        self.notifi_id = notifi_id
        self.title = title
        self.body = body
    }
}
