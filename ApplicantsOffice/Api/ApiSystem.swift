//
//  ApiSystem.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 25/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

extension Api{
    
    static func getSystemStatistic(){
        let parameters: Parameters = [
            "app_version": Bundle.main.infoDictionary!["CFBundleShortVersionString"] ?? "",
            "manufacturer": "Apple Inc.",
            "os": "iOS " + UIDevice.current.systemVersion,
            "model": UIDevice.modelName
        ]
        Alamofire.request(Api.URL + "system/statistic", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: Api.getHeaders()).responseJSON{ response in
        }
    }
    
    static func checkForUpdate() -> Void{
        request("https://#/ios-app/version.php").response { response in
            guard
                let data = response.data,
                let varsionApp = String(data: data, encoding: .utf8)
                else { return }
            if varsionApp != Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String{
                Notification.update()
            }
        }
    }
}
