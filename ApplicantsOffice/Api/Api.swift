//
//  Api.swift
//  applicant'sOffice
//
//  Created by Евгений Балашов on 21/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit
import Alamofire
import Fabric
import Crashlytics
import RealmSwift

class Api{
    
    static let VERSION: String = "v1"
    static let URL:String = "https://#/" + VERSION + "/"
    static let USER_AGENT: String = "MCART3"
    static let SRV:String = "balashov"
    static let APP:String = "#"
    
    struct ErrorCode {
        static let errorMegaWarning = 1000
        static let errorInternal = 100
        static let errorTaskNotFound = 26
        static let errorCellPhoneDoNotSet = 61
        static let errorNeedAutorization = 5
        static let errorKeyWrong = 9
        static let errorPhoneNotFound = 7
        static let errorPhoneDisabled = 22
        static let errorAuthorizationKeyDoNotSet = 4
        static let errorSessionIsExpired = 11
        static let errorAuthNotFound = 8
        static let errorDataIsNotJson = 32
        static let errorSessionIsNotValid = 12
    }
    
    static func getUUID()->String {
        return UIDevice.current.identifierForVendor!.uuidString
    }
    
    static func getHeaders() -> HTTPHeaders {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json; charset=UTF-8",
            "Accept-Encoding": "gzip",
            "User-Agent": Api.USER_AGENT,
            "ENV": "false",
            "srv": Api.SRV,
            "device_uuid": getUUID(),
            "session_token": Preferences.getInstans().getSessionToken(),
            "APP": Api.APP
        ]
        return headers
    }
    
    static func requestInit(parameters: Parameters) -> Void {
        
        Alamofire.request(Api.URL + "auth/init", method:.post, parameters: parameters, encoding: JSONEncoding.default, headers:Api.getHeaders()).responseJSON { response in
            switch response.result {
            case .success:
                debugPrint(response)
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    if (JSON.object(forKey: "status") as! Bool){
                        
                        let responseJson = JSON.object(forKey: "response") as! NSDictionary
                        
                        let sessionToken:String = responseJson.object(forKey: "session_token") as! String
                        Preferences.getInstans().setSessionToken(sessionToken: sessionToken)
                        
                        let sessionTimeLeft:Int = responseJson.object(forKey: "session_time_left") as! Int
                        Preferences.getInstans().setSessionTimeLeft(sessionTimeLeft: sessionTimeLeft)
                        
                        let index = parameters.index(forKey: "cell_phone")
                        Preferences.getInstans().setPhoneNumber(phoneNumber: parameters[index!].value as! String)
                        
                        Api.updateSession()
                        
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func parseResponse(response: DataResponse<Any>) -> NSDictionary? {
        switch response.result {
        case .success:
            
            if let result = response.value {
                let JSON = result as! NSDictionary
                if (JSON.object(forKey: "status") as! Bool) {
                    return JSON
                }else{
                    debugPrint("JSON.object Error", JSON.object(forKey: "error") ?? 0)
                    
                    if (JSON.object(forKey: "error") as? Int == Api.ErrorCode.errorSessionIsExpired){
                        Api.updateSession()
                    }
                    
                    if (JSON.object(forKey: "error") as? Int == Api.ErrorCode.errorSessionIsNotValid){
                        
                        let parameters: Parameters = [
                            "cell_phone": Preferences.getInstans().getPhoneNumber()
                        ]
                        
                        Api.requestInit(parameters: parameters)
                        
                    }
                    
                    if (JSON.object(forKey: "error") as? Int == Api.ErrorCode.errorMegaWarning){
                        Preferences.removeUserDefaults()
                        Database.shared.realm.deleteAll()
                        exit(0)
                    }
                }
            }
        case .failure(let error):
            print("responseError",error)
        }
        
        return nil
    }
    
    
    
}

public extension UIDevice {
    
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        func mapToDevice(identifier: String) -> String {
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod touch (5th generation)"
            case "iPod7,1":                                 return "iPod touch (6th generation)"
            case "iPod9,1":                                 return "iPod touch (7th generation)"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone11,2":                              return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
            case "iPhone11,8":                              return "iPhone XR"
            case "iPhone12,1":                              return "iPhone 11"
            case "iPhone12,3":                              return "iPhone 11 Pro"
            case "iPhone12,5":                              return "iPhone 11 Pro Max"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad (3rd generation)"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad (4th generation)"
            case "iPad6,11", "iPad6,12":                    return "iPad (5th generation)"
            case "iPad7,5", "iPad7,6":                      return "iPad (6th generation)"
            case "iPad7,11", "iPad7,12":                    return "iPad (7th generation)"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad11,4", "iPad11,5":                    return "iPad Air (3rd generation)"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad mini 4"
            case "iPad11,1", "iPad11,2":                    return "iPad mini (5th generation)"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch)"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }
        
        return mapToDevice(identifier: identifier)
    }()
    
}
