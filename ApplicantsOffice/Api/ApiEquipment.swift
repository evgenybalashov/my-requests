//
//  ApiEquipment.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 25/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

extension Api{
    
    static func getEquipment(completion: @escaping (_ error: Error?, _ equipments: [Equipment]?)->Void) {
        
        let parameters: Parameters = [
            "latitude": Location.getInstans().getLatitude(),
            "longitude": Location.getInstans().getLongitude(),
            "accuracy": Location.getInstans().getAccuracy()
        ]
        
        Alamofire.request(Api.URL + "equipment/list", method: .get, parameters: parameters, headers: Api.getHeaders()).responseJSON{ response in
            let parsedResponse = Api.parseResponse(response: response)
            if parsedResponse != nil {
                
                let equipmentJson = JSON(parsedResponse as Any)
                guard let data = equipmentJson["response"].array else {
                    completion(nil, nil)
                    return
                }
                
                var equipments = [Equipment]()
                data.forEach({
                    if let item = $0.dictionary, let equipment = Equipment.init(item: item) {
                        equipments.append(equipment)
                    }
                })
                completion(nil, equipments)
            }
        }
    }
}
