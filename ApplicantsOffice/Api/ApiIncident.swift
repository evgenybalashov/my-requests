//
//  ApiIncident.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 25/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import SPAlert

extension Api{
    
    static func getIncidentListHash(completion: @escaping (_ error: Error?, _ hash:[Hash]?  )->Void) {
        
        Alamofire.request(Api.URL + "incident/list-hashes", method:.get, headers: Api.getHeaders()).responseJSON { response in
            let parsedResponse = Api.parseResponse(response: response)
            if parsedResponse != nil {
                let hashJson = JSON(parsedResponse as Any)
                
                guard let data = hashJson["response"].array else {
                    completion(nil, nil)
                    return
                }
                
                var hashs = [Hash]()
                
                for item in data {
                    if let item = item.dictionary, let hash = Hash.init(item: item) {
                        hashs.append(hash)
                    }
                }
                
                completion(nil, hashs)
            }else{
                SPAlert.present(message: "Нет подключения")
            }
        }
    }
    
    
    static func getIncident (id: Int, hash: Int, completion: @escaping (_ error: Error?, _ incident: Incident?)->Void){
        
        let parameters: Parameters = [
            "id": id
        ]
        
        Alamofire.request(Api.URL + "incident/get", method: .get, parameters: parameters,
                          headers: Api.getHeaders()).responseJSON{ response in
                            let parsedResponse = Api.parseResponse(response: response)
                            if parsedResponse != nil {
                                
                                let incidentApi = parsedResponse?.object(forKey: "response") as! NSDictionary
                                
                                let incident = Incident.init(
                                    incident_id: id,
                                    crc32_hash: hash,
                                    status_title: incidentApi["status_title"] as! String,
                                    timestamp_create: incidentApi["timestamp_create"] as! Int,
                                    equipment_title: incidentApi["equipment_title"] as! String,
                                    equipment_uin: incidentApi["equipment_uin"] as? String,
                                    equipment_markplace: incidentApi["equipment_markplace"] as? String,
                                    in_work: incidentApi["in_work"] as! Bool,
                                    timestamp_close: incidentApi["timestamp_close"] as? Int,
                                    problems: incidentApi["problems"] as? String,
                                    additional_info: incidentApi["additional_info"] as? String,
                                    is_need_rating: incidentApi["is_need_rating"] as! Bool,
                                    rating: incidentApi["rating"] as? Int,
                                    rating_comment: incidentApi["rating_comment"] as? String,
                                    photo: incidentApi["photo"] as? String)
                                
                                print("incident", incident)
                                completion(nil, incident)
                                
                            }else{
                                print("nil")
                            }
                            
        }
    }
    
    
    class func setIncidentRating(parameters: Parameters, completion: @escaping (_ error: Error?, _ success: String?  )->Void){
        
        Alamofire.request(Api.URL + "incident/set-rating", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: Api.getHeaders()).responseJSON{ response in
            
            let parsedResponse = Api.parseResponse(response: response)
            if parsedResponse != nil {
                
                
                let createApi = parsedResponse?.object(forKey: "response") as! NSString
                
                var success = String()
                success = createApi as String
                print("success", success)
                completion(nil, success)
            }
            
        }
    }
    
    static func getIncidentCreate(parameters: Parameters, completion: @escaping (_ error: Error?, _ answer: Int?  )->Void){
        
        Alamofire.request(Api.URL + "incident/create", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: Api.getHeaders()).responseJSON{ response in
            let parsedResponse = Api.parseResponse(response: response)
            if parsedResponse != nil {
                
                
                let createApi = parsedResponse?.object(forKey: "response") as! NSInteger
                
                var answer = Int()
                answer = createApi
                print("answer", answer)
                completion(nil, answer)
            }
            
        }
    }
}
