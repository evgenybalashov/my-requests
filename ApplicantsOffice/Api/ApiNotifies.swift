//
//  ApiNotifies.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 15/11/2019.
//  Copyright © 2019 example. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import SPAlert


extension Api{
    
    static func getNotifiList(completion: @escaping (_ error: Error?, _ notifi:[Notifi]?)->Void){
        
        Alamofire.request(Api.URL + "notifies/list", method: .get, headers: Api.getHeaders()).responseJSON{ response in
            let parsedResponse = Api.parseResponse(response: response)
            if parsedResponse != nil {
                print("parsedResponse", parsedResponse)
                let notifiJson = JSON(parsedResponse as Any)
                
                guard let data = notifiJson["response"].array else {
                    completion(nil, nil)
                    return
                }
                print("data",data)
                var notifis = [Notifi]()
                
                for item in data {
                    if let item = item.dictionary, let notifi = Notifi.init(item: item) {
                        notifis.append(notifi)
                    }
                }
                completion(nil, notifis)
            }else{
                SPAlert.present(message: "Нет подключения")
            }
            
        }
    }
    
    
    static func getComplete(parameters: Parameters, completion: @escaping (_ error: Error?, _ success:String?)->Void){
        
        Alamofire.request(Api.URL + "notifies/complete", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: Api.getHeaders()).responseJSON{ response in
            let parsedResponse = Api.parseResponse(response: response)
            if parsedResponse != nil {
                
                let successApi = parsedResponse?.object(forKey: "response") as! NSString
                
                var success = String()
                success = successApi as String                
            }
            
        }
    }
}

