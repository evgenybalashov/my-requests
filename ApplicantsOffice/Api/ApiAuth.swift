//
//  ApiAuth.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 08/11/2019.
//  Copyright © 2019 example. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import SPAlert

extension Api {
    
    static func updateSession()-> Void{
        let parameters: Parameters = [:]
        
        Alamofire.request(URL + "auth/session", method:.post,parameters: parameters, encoding: JSONEncoding.default,
                          headers:Api.getHeaders()).responseJSON { response in
                            let parsedResponse = Api.parseResponse(response: response)
                            if parsedResponse != nil {
                                let responseJson = parsedResponse?.object(forKey: "response") as! NSDictionary
                                
                                let sessionToken:String = responseJson.object(forKey: "session_token") as! String
                                Preferences.getInstans().setSessionToken(sessionToken: sessionToken)
                                
                                let sessionTimeLeft:Int = responseJson.object(forKey: "session_time_left") as! Int
                                Preferences.getInstans().setSessionTimeLeft(sessionTimeLeft: sessionTimeLeft)
                                
                                let userName:String = responseJson.object(forKey: "user_name") as! String
                                Preferences.getInstans().setUserName(userName: userName)
                            }
                            
        }
    }
}
