//
//  ApiFeedback.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 14/11/2019.
//  Copyright © 2019 example. All rights reserved.
//


import Alamofire
import SwiftyJSON

extension Api{
    
    
    static func feedback(parameters: Parameters, completion: @escaping (_ error: Error?, _ feedback: String?)->Void){
        Alamofire.request(Api.URL + "system/feedback", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: Api.getHeaders()).responseJSON{ response in
            let parsedResponse = Api.parseResponse(response: response)
            if parsedResponse != nil {
                
                var feedback = String()
                feedback = "Отправлено"
                completion(nil, feedback)
            }
        }
    }
    
}
