//
//  UILabelExtenshion.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 01/11/2019.
//  Copyright © 2019 example. All rights reserved.
//
import UIKit

extension UILabel {
    
    func setMultiSize(_ text: String, width: UIFont.FontWeight, positions: [FormatPosition], base: CGFloat, higlighted: CGFloat, higlightedColor: UIColor) {
        
        self.font = UIFont.system(weight: width, size: base)
        
        let title = NSMutableAttributedString.init(string: text)
        for position in positions {
            title.addAttributes(
                [
                    NSAttributedString.Key.font : UIFont.system(weight: width, size: higlighted),
                    NSAttributedString.Key.foregroundColor : higlightedColor
                ],
                range: NSRange.init(location: position.start, length: position.length)
            )
        }
        
        self.attributedText = title
    }
    
    func setMultiColors(_ text: String, positions: [FormatPosition], textColor: UIColor, secondTextColor: UIColor) {
        
        self.textColor = textColor
        
        let title = NSMutableAttributedString.init(string: text)
        for position in positions {
            title.addAttributes(
                [
                    NSAttributedString.Key.foregroundColor : secondTextColor
                ],
                range: NSRange.init(location: position.start, length: position.length)
            )
        }
        
        self.attributedText = title
    }
}

struct FormatPosition {
    
    var start: Int
    var length: Int
}
