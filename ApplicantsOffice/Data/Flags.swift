//
//  File.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 23/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import Foundation
import UIKit

enum Flags {
    
    static var useStorkPresentation: Bool {
        return UIDevice.current.userInterfaceIdiom != .pad
    }
    
    static var isConfirm: Bool {
         get {
             if let value = Database.userDefaults.value(forKey: "isConfirm") as? Bool {
                 return value
             } else {
                 return true
             }
         }
         set {
             Database.userDefaults.set(newValue, forKey: "isConfirm")
         }
     }
    
    static var audioEnabled: Bool {
        get {
            if let value = Database.userDefaults.value(forKey: "audioEnabled") as? Bool {
                return value
            } else {
                return true
            }
        }
        set {
            Database.userDefaults.set(newValue, forKey: "audioEnabled")
        }
    }
}
