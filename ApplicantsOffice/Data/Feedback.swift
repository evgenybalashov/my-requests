//
//  Feedback.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 14/11/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit
import SPAlert
import Eureka
import Alamofire

enum Feedback{
    
    static func open(on controller: FormViewController) {
        
        controller.showFeedBackDialog(title: "Обратная связь",
                                      subtitle: "Ваше предложение/проблема",
                                      actionTitle: "Отправить",
                                      cancelTitle: "Отмена",
                                      inputKeyboardType: .default)
        { (input:String?) in
            if(input!.isEmpty){
                SPAlert.present(message: "Не отправлено. Сообщение не должно быть пустым")
            }else{
                let parameters: Parameters = [
                    "message": input!
                ]
                if Connectivity.isConnectedToInternet{
                    Api.feedback(parameters: parameters) { (error: Error?, feedback: String?) in
                        if let feedback = feedback{
                            SPAlert.present(title: feedback, message: nil, preset: .done)
                        }
                    }
                }else{
                    SPAlert.present(message: Text.no_connection)
                }
            }
        }
    }
    
    
    
}
