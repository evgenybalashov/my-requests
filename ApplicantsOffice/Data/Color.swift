//
//  Color.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 31/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit

enum Color {
    
    fileprivate static var white: UIColor { return UIColor.white }
    fileprivate static var black: UIColor { return UIColor.black }
    static var base: UIColor { return UIColor.init(hex: "#00A65A") }
    fileprivate static var lightGray: UIColor { return UIColor.init(hex: "F8F7FC") }
    fileprivate static var darkGray: UIColor { return UIColor.init(hex: "515B66") }
    static var starGray: UIColor { return UIColor.init(hex: "ffd500") }
    static var orange: UIColor { return UIColor.init(hex: "D48B11") }
    static var orangeNew: UIColor { return UIColor.init(hex: "DAC20D") }
    static var gradient: (first: UIColor, second: UIColor) {
//        return (first: UIColor.init(hex: "FD2D55"), second: UIColor.init(hex: "FD592E"))
        return (first: UIColor.init(hex: "00A65A"), second: UIColor.init(hex: "#69a65a"))
    }
    
    enum Assest {
        
        static var bakground: UIColor {
            if #available(iOS 11.0, *) {
                return UIColor.init(named: "Background") ?? Color.white
            } else {
                return Color.white
            }
        }
        
        static var dark_bakground: UIColor {
            if #available(iOS 11.0, *) {
                return UIColor.init(named: "Dark Background") ?? Color.white
            } else {
                return Color.white
            }
        }
        
        static var text: UIColor {
            if #available(iOS 11.0, *) {
                return UIColor.init(named: "Text") ?? Color.black
            } else {
                return Color.black
            }
        }
        
        static var area: UIColor {
            if #available(iOS 11.0, *) {
                return UIColor.init(named: "Area") ?? Color.lightGray
            } else {
                return Color.lightGray
            }
        }
        
        static var text_in_area: UIColor {
            if #available(iOS 11.0, *) {
                return UIColor.init(named: "Text in Area") ?? Color.darkGray
            } else {
                return Color.darkGray
            }
        }
        
        static var indicator: UIColor {
            if #available(iOS 11.0, *) {
                return UIColor.init(named: "Indicator") ?? Color.lightGray
            } else {
                return Color.lightGray
            }
        }
        
        static var separator: UIColor {
            if #available(iOS 11.0, *) {
                return UIColor.init(named: "Separator") ?? Color.lightGray
            } else {
                return Color.lightGray
            }
        }
        
        static var cell_separator: UIColor {
            if #available(iOS 11.0, *) {
                return UIColor.init(named: "Cell Separator") ?? Color.lightGray
            } else {
                return Color.lightGray
            }
        }
    }
}

