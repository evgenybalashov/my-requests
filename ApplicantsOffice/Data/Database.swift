//
//  Database.swift
//  applicant'sOffice
//
//  Created by Евгений Балашов on 21/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit
import RealmSwift
import MessageUI

class Database{
    
//    static func getInstance() -> Realm {
//        if self.realm != nil {
//            return self.realm
//        }
//
//        let config = Realm.Configuration(fileURL: self.realmURL, schemaVersion: 3, migrationBlock: { (migration, oldSchemaVersion) in })
//        let realm = try! Realm(configuration: config)
//        self.realm = realm
//        return realm
//    }
    private init(){}
//
    static let shared = Database()
    
    private static let identificator: String = "group.com.example.applicant-sOffice.database"
       
    static var userDefaults: UserDefaults {
        return UserDefaults(suiteName: self.identificator)!
    }
//    
//    fileprivate static var realmURL: URL {
//        return FileManager.default
//            .containerURL(forSecurityApplicationGroupIdentifier: self.identificator)!
//            .appendingPathComponent("default.realm")
//    }
    
    
    var realm = try! Realm()
    
//    static func create<T: Object>(_ object: T){
//        do {
//            try Database.realm.write {
//                print("OBJECT", object)
//                Database.realm.add(object)
//            }
//        } catch {
//            print(error)
//        }
//    }
//
//    func update<T: Object>(_ object: T, with dictionary: [String: Any?]){
//        do {
//            try Database.realm.write {
//                for (key, value) in dictionary{
//                    print("OBJECT", object)
//                    object.setValue(value, forKey: key)
//                }
//            }
//        } catch {
//            print(error)
//        }
//    }
//
//    func delete<T: Object>(_ object: T){
//        do {
//            try Database.realm.write {
//                print("OBJECT", object)
//                Database.realm.delete(object)
//            }
//        } catch {
//            print(error)
//        }
//    }
    
        func deleteAll(){
            do {
                try Database.shared.realm.write {
                    Database.shared.realm.deleteAll()
                }
            } catch {
                print(error)
            }
        }
}

extension Data {

    func toTempFile(fileName: String, extenshion: String) -> URL? {
        
        let directory = FileManager.default.temporaryDirectory
        
        let fileURL = directory.appendingPathComponent(fileName + "." + extenshion)
        
        do {
            try self.write(to: fileURL)
            return fileURL
        }
        catch {
            print("Error writing the file: \(error.localizedDescription)")
        }
        
        return nil
    }
}
