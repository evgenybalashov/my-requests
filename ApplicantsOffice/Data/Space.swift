//
//  Space.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 31/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit

enum Space {
    
    static var vertical: CGFloat {
        return 24
    }
    
    static var side: CGFloat {
        return 19
    }
    
    static var modalSide: CGFloat {
        return 27
    }
}

enum Style {
    
    static var buttonHeight: CGFloat {
        return 58
    }
    
    static var cornerRadius: CGFloat {
        return 13
    }
}
