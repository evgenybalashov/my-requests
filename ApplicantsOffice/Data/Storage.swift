//
//   Storage.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 31/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit
import RealmSwift

enum Storage {
        
    static func save(incident: Incident, old: Incident? = nil){
        let realm = Database.shared.realm

                let incidentObject = IncidentStorage()
                incidentObject.incident_id = incident.incident_id
                incidentObject.crc32_hash = incident.crc32_hash
                incidentObject.status_title = incident.status_title
                incidentObject.timestamp_create = incident.timestamp_create
                incidentObject.equipment_title = incident.equipment_title
                incidentObject.equipment_uin = incident.equipment_uin
                incidentObject.equipment_markplace = incident.equipment_markplace
                incidentObject.in_work = incident.in_work
                incidentObject.timestamp_close = incident.timestamp_close ?? 0
                incidentObject.additional_info = incident.additional_info
                incidentObject.rating = incident.rating
                incidentObject.rating_comment = incident.rating_comment
                incidentObject.problems = incident.problems
                incidentObject.photo = ""
                incidentObject.is_need_rating = incident.is_need_rating
        
                try! realm.write {
                    realm.add(incidentObject)
                }
    }
    
    
    static func createIncident(object: Incident){
        let realm = Database.shared.realm
        let incidents = realm.objects(IncidentStorage.self)
        do {
            if let incident = incidents.first {
            try realm.write {
                    incident.incident_id = object.incident_id
                    incident.crc32_hash = object.crc32_hash
                    incident.status_title = object.status_title
                    incident.timestamp_create = object.timestamp_create
                    incident.equipment_title = object.equipment_title
                    incident.equipment_uin = object.equipment_uin
                    incident.in_work = object.in_work
                    incident.timestamp_close = object.timestamp_close ?? 0
                    incident.additional_info = object.additional_info
                    incident.rating = object.rating
                    incident.rating_comment = object.rating_comment
                    incident.photo = ""
                    incident.is_need_rating = object.is_need_rating

                print("saveIncident", incident)
                try! realm.write {
                    realm.add(incident)
                }
            }
        }
        } catch {
            print(error)
        }
    }
    
    
     static func delete(incident_id: Int) {
        
        let realm = Database.shared.realm
        let incident = realm.objects(IncidentStorage.self).filter("incident_id == \(incident_id)")

           try! realm.write {
//               object.isDeleted = true
               realm.delete(incident)
           }
       }
    
    
    static func update(incident_id: Int, with dictionary: [String: Any?]){
        let realm = Database.shared.realm
        let incident = realm.objects(IncidentStorage.self).filter("incident_id == \(incident_id)")
        do {
            try realm.write {
                print("rating", incident_id, dictionary)
                for (key, value) in dictionary{
                    print("OBJECT", incident)
                    incident.setValue(value, forKey: key)
                }
            }
        } catch {
            print(error)
        }
    }
    
    
    
    static func incidents() -> [Incident] {
        
        let realm = Database.shared.realm
        let incidentsObjects = realm.objects(IncidentStorage.self).filter("in_work == true AND isDeleted == false")
        var incidents: [Incident] = []
        incidentsObjects.forEach { (object) in
            let incident = Incident.init(
                incident_id: object.incident_id,
                crc32_hash: object.crc32_hash,
                status_title: object.status_title,
                timestamp_create: object.timestamp_create,
                equipment_title: object.equipment_title,
                equipment_uin: object.equipment_uin,
                equipment_markplace: object.equipment_markplace,
                in_work: object.in_work,
                timestamp_close: object.timestamp_close,
                problems: object.problems,
                additional_info: object.additional_info,
                is_need_rating: object.is_need_rating,
                rating: object.rating,
                rating_comment: object.rating_comment,
                photo: object.photo)

            incidents.append(incident)
        }
        
        return self.sorted(incidents)
    }
    
    
    static func incidentNoInternet() -> [Incident] {
        
        let realm = Database.shared.realm
        let incidentsObjects = realm.objects(IncidentStorage.self).filter("incident_id == \(0)")
        var incidents: [Incident] = []
        incidentsObjects.forEach { (object) in
            let incident = Incident.init(
                incident_id: object.incident_id,
                crc32_hash: object.crc32_hash,
                status_title: object.status_title,
                timestamp_create: object.timestamp_create,
                equipment_title: object.equipment_title,
                equipment_uin: object.equipment_uin,
                equipment_markplace: object.equipment_markplace,
                in_work: object.in_work,
                timestamp_close: object.timestamp_close,
                problems: object.problems,
                additional_info: object.additional_info,
                is_need_rating: object.is_need_rating,
                rating: object.rating,
                rating_comment: object.rating_comment,
                photo: object.photo)

            incidents.append(incident)
        }
        
        return self.sorted(incidents)
    }
    
    private static func sorted(_ incidents: [Incident]) -> [Incident] {
        
        return incidents.sorted{ $0.timestamp_create > $1.timestamp_create }
    }
    
    static func archiveIncidents() -> [Incident] {
        
        let realm = Database.shared.realm
        let incidentsObjects = realm.objects(IncidentStorage.self).filter("in_work == false AND isDeleted == false")
        var incidents: [Incident] = []
        incidentsObjects.forEach { (object) in
            let incident = Incident.init(
                incident_id: object.incident_id,
                crc32_hash: object.crc32_hash,
                status_title: object.status_title,
                timestamp_create: object.timestamp_create,
                equipment_title: object.equipment_title,
                equipment_uin: object.equipment_uin,
                equipment_markplace: object.equipment_markplace,
                in_work: object.in_work,
                timestamp_close: object.timestamp_close,
                problems: object.problems,
                additional_info: object.additional_info,
                is_need_rating: object.is_need_rating,
                rating: object.rating,
                rating_comment: object.rating_comment,
                photo: object.photo)

            incidents.append(incident)
        }
        
        return self.sorted(incidents)
    }    
}
