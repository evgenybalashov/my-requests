//
//  Location.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 25/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import CoreLocation

class Location: NSObject, CLLocationManagerDelegate {
    
    private let locationManager = CLLocationManager()
    
    static var instanse = Location()
    
    static func getInstans()-> Location{
        return instanse
    }
    
    override init() {
        super.init()
        
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.allowsBackgroundLocationUpdates = true
    }
    
    func getLatitude() -> Double{
        let latitude = locationManager.location?.coordinate.latitude
        if latitude != nil{
            return latitude!
        }
        return 0
    }
    
    func getLongitude() -> Double{
        let longitude = locationManager.location?.coordinate.longitude
        if longitude != nil{
            return longitude!
        }
        return 0
    }
    
    func getAccuracy() -> Int{
        //        var accuracy = locationManager.desiredAccuracy
        //        accuracy = kCLLocationAccuracyHundredMeters
        //        return Int(accuracy)
        
        let accuracy = locationManager.location?.horizontalAccuracy
        if accuracy != nil{
            return Int(accuracy!)
        }
        return 0
    }
    
}
