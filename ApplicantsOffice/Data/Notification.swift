//
//  Notifications.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 14/11/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit
import UserNotifications

enum Notification {
    
    static var closeActionID = "close-incident"
    
    static func test() {
        
        var notification = SPLocalNotification(after: 0.1, text: Text.example_push_subtitle)
        notification.title = Text.example_push_title
        notification.soundEnabled = Flags.audioEnabled
        notification.category = SPLocalNotificationCategory(
            identifier: "test-notification-category",
            summary: "%u \(Text.more_test_notifications)"
        )
        notification.add()
    }
    
    static func remove(incident id: String) {
        let center = UNUserNotificationCenter.current()
        center.removePendingNotificationRequests(withIdentifiers: [id])
    }
    
    static func add(incident: Incident) {
        
        guard let incDate = incident.timestamp_close else { return }
        
        let pushDate = Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: Date(timeIntervalSince1970: TimeInterval(incDate)))
        let timeIntervalOptional = pushDate?.timeIntervalSinceNow
        
        guard let timeInterval = timeIntervalOptional else { return }
        guard timeInterval > 0 else { return }
        
        var notification = SPLocalNotification(after: timeInterval, text: "???")
//        notification.identificator = incident.incident_id
        //
        //        if debt.type == .create {
        //            notification.t ext = Text.todayActionCreateDebt(user: debt.user.name, amount: debt.formattedAmount)
        //        }
        //
        //        if debt.type == .take {
        //            notification.text = Text.todayActionTakeDebt(user: debt.user.name, amount: debt.formattedAmount)
        //        }
        
        notification.category = SPLocalNotificationCategory(identifier: "due-incident", summary: Text.incident)
        notification.category?.actions = [
            UNNotificationAction(identifier: self.closeActionID, title: Text.incident_close, options: [])
        ]
        notification.add()
    }
    
    static func createNotification(){
        
        let content = UNMutableNotificationContent()
        content.title = Text.example_push_title
        content.body = Text.example_push_subtitle
        content.sound = UNNotificationSound.default
        
        let triger = UNTimeIntervalNotificationTrigger(timeInterval: 3.0, repeats: false)
        
        let request = UNNotificationRequest(identifier: String(Int.random(in: 0..<9999)), content: content, trigger: triger)
        
        UNUserNotificationCenter.current().add(request) { (error) in
            print(error as Any)
        }
    }
    
    static func createIncident(title: String, body: String){
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.sound = UNNotificationSound.default
        
        let triger = UNTimeIntervalNotificationTrigger(timeInterval: 3.0, repeats: false)
        
        let request = UNNotificationRequest(identifier: String(Int.random(in: 0..<9999)), content: content, trigger: triger)
        
        UNUserNotificationCenter.current().add(request) { (error) in
            print(error as Any)
        }
    }
    
    static func update(){
       
           let content = UNMutableNotificationContent()
           content.title = "Доступна новая версия приложения"
           content.body = "Нажмите, чтобы обновить сейчас"
           content.sound = UNNotificationSound.default
           
           let triger = UNTimeIntervalNotificationTrigger(timeInterval: 3.0, repeats: false)
           
           let request = UNNotificationRequest(identifier: String(Int.random(in: 0..<9999)), content: content, trigger: triger)
           
           UNUserNotificationCenter.current().add(request) { (error) in
               print(error as Any)
           }
       }
}
