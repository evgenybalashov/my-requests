//
//  Text.swift
//  applicant'sOffice
//
//  Created by Евгений Балашов on 21/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit

enum Text{
    
    static var menu: String {
        switch SPLocale.current {
        case .en:
            return "Menu"
        case .ru:
            return "Меню"
        }
    }
    
    static var archive: String {
        switch SPLocale.current {
        case .en:
            return "Archive"
        case .ru:
            return "Архив"
        }
    }
    
    static var incidents: String {
        switch SPLocale.current {
        case .en:
            return "Incidents"
        case .ru:
            return "Инциденты"
        }
    }
    
    static var incident: String {
        switch SPLocale.current {
        case .en:
            return "Incident"
        case .ru:
            return "Инцидент"
        }
    }
    
    
    static var cancel: String {
        switch SPLocale.current {
        case .en:
            return "Cancel"
        case .ru:
            return "Отмена"
        }
    }
    
    static var repeal: String {
        switch SPLocale.current {
        case .en:
            return "Cancel"
        case .ru:
            return "Закрыть"
        }
    }
    
    
    static var view_incident: String {
        switch SPLocale.current {
        case .en:
            return "View incident"
        case .ru:
            return "Просмотр инцидента"
        }
    }
    
    static var new_incidents: String {
        switch SPLocale.current {
        case .en:
            return "New incident"
        case .ru:
            return "Новый инцидент"
        }
    }
    
    static var create: String {
        switch SPLocale.current {
        case .en:
            return "Create"
        case .ru:
            return "Создать"
        }
    }
    
    static var estimate: String {
        switch SPLocale.current {
        case .en:
            return "Estimate"
        case .ru:
            return "Оценить"
        }
    }
    
    static var added: String {
        switch SPLocale.current {
        case .en:
            return "Added"
        case .ru:
            return "Создан"
        }
    }
    
    static var updated: String {
        switch SPLocale.current {
        case .en:
            return "Updated"
        case .ru:
            return "Обновлено"
        }
    }
    
    static var no_equipment_specified: String {
        switch SPLocale.current {
        case .en:
            return "No equipment"
        case .ru:
            return "Не указано оборудование"
        }
    }
    
    static var equipment: String {
        switch SPLocale.current {
        case .en:
            return "Equipment"
        case .ru:
            return "Оборудование"
        }
    }
    
    static var equipment_description: String {
        switch SPLocale.current {
        case .en:
            return "Enter uin or sn"
        case .ru:
            return "Введите УИН или серийный номер"
        }
    }
    
    static var empty: String {
        switch SPLocale.current {
        case .en:
            return "Empty"
        case .ru:
            return "Пусто"
        }
    }
    
    static var no_incident: String {
        switch SPLocale.current {
        case .en:
            return "No incindets"
        case .ru:
            return "Нет инцидентов"
        }
    }
    
    static var no_equipments: String {
        switch SPLocale.current {
        case .en:
            return "No equipments"
        case .ru:
            return "Нет оборудования"
        }
    }
    
    static var incident_close: String {
        switch SPLocale.current {
        case .en:
            return "Close incident"
        case .ru:
            return "Инцидент закрыт"
        }
    }
    
    static var tap_to_change: String {
        switch SPLocale.current {
        case .en:
            return "Tap to change"
        case .ru:
            return "Нажмите чтобы изменить"
        }
    }
    
    static var choose_equipment: String {
        switch SPLocale.current {
        case .en:
            return "Choose a equipment"
        case .ru:
            return "Выберите оборудование"
        }
    }
    
    static var equipment_list_will_open: String {
        switch SPLocale.current {
        case .en:
            return "The equipment list will open"
        case .ru:
            return "Откроется список оборудований"
        }
    }
    
    static var input_example: String {
        switch SPLocale.current {
        case .en:
            return "Enter uin or sn"
        case .ru:
            return "Введите uin или sn"
        }
    }
    
    static var problem: String {
        switch SPLocale.current {
        case .en:
            return "Description of problem"
        case .ru:
            return "Описание проблемы"
        }
    }
    
    static var clear: String {
        switch SPLocale.current {
        case .en:
            return "Clear"
        case .ru:
            return "Очистить"
        }
    }
    
    
    static var add: String {
        switch SPLocale.current {
        case .en:
            return "Add"
        case .ru:
            return "Добавить"
        }
    }
    
    static var note_for_incident_placeholder: String {
        switch SPLocale.current {
        case .en:
            return "Describe in detail the problem"
        case .ru:
            return "Опишите подробно проблему"
        }
    }
    
    static var unsaved_changes: String {
        switch SPLocale.current {
        case .en:
            return "There are unsaved changes"
        case .ru:
            return "Есть несохраненные изменения"
        }
    }
    
    static var unsaved_changes_decsription: String {
        switch SPLocale.current {
        case .en:
            return "Confirmation is disabled in the settings."
        case .ru:
            return "Измененения будут потеряны."
        }
    }
    
    static var still_close: String {
        switch SPLocale.current {
        case .en:
            return "Still close"
        case .ru:
            return "Всё равно закрыть"
        }
    }
    
    static var no_description: String {
        switch SPLocale.current {
        case .en:
            return "No equipment"
        case .ru:
            return "Не указано описание проблемы"
        }
    }
    
    static var photo: String {
        switch SPLocale.current {
        case .en:
            return "Photo"
        case .ru:
            return "Фото"
        }
    }
    
    static var photo_title: String {
        switch SPLocale.current {
        case .en:
            return "Add photo"
        case .ru:
            return "Добавить фото"
        }
    }
    
    static var photo_message: String {
        switch SPLocale.current {
        case .en:
            return "Choose how to add ..."
        case .ru:
            return "Выберите, как добавить..."
        }
    }
    
    static var camera: String {
        switch SPLocale.current {
        case .en:
            return "Camera"
        case .ru:
            return "Камера"
        }
    }
    
    static var gallery: String {
        switch SPLocale.current {
        case .en:
            return "Gallery"
        case .ru:
            return "Галлерея"
        }
    }
    
    static var example_push_subtitle: String {
        switch SPLocale.current {
        case .en:
            return "If you see this, notifications work correctly."
        case .ru:
            return "Если вы это видите, значит уведомления работают."
        }
    }
    
    static var example_push_title: String {
        switch SPLocale.current {
        case .en:
            return "Push Enabled"
        case .ru:
            return "Уведомления включены"
        }
    }
    
    static var more_test_notifications: String {
        switch SPLocale.current {
        case .en:
            return "more test notifications"
        case .ru:
            return "тестовых"
        }
    }
    
    static var error: String {
        switch SPLocale.current {
        case .en:
            return "Error"
        case .ru:
            return "Ошибка"
        }
    }
    
    static var scan_again: String {
        switch SPLocale.current {
        case .en:
            return "Scan again"
        case .ru:
            return "Проверить снова"
        }
    }
    
    static var no_connection: String {
        switch SPLocale.current {
        case .en:
            return "No connection"
        case .ru:
            return "Нет соединения"
        }
    }
}
