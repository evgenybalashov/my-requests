////
////  Analitics.swift
////  applicantsOffice
////
////  Created by Евгений Балашов on 01/11/2019.
////  Copyright © 2019 example. All rights reserved.
////
//
//import UIKit
//
//enum Analitics {
//
//    case edit_incident
//    case close_incident
//    case remove_uncident
//    case clear_history
//    case add_examples
//    case lock
//    case unlock
//    case rescan
//    case restore_from_backup
//
//    var id: String {
//        switch self {
//        case .edit_incident:
//            return "edit_incident"
//        case .close_incident:
//            return "close_incident"
//        case .remove_debt:
//            return "remove_debt"
//        case .remove_user:
//            return "remove_user"
//        case .clear_history:
//            return "clear_history"
//        case .tap_see_note:
//            return "tap_see_note"
//        case .tap_add_note:
//            return "tap_add_note"
//        case .add_examples:
//            return "add_examples"
//        case .lock:
//            return "lock"
//        case .unlock:
//            return "unlock"
//        case .rescan:
//            return "rescan"
//        case .repayment:
//            return "repayment"
//        case .save_backup:
//            return "create"
//        case .restore_from_backup:
//            return "restore"
//        }
//    }
//
//    static func newIncident(type: IncidentType, withNote: Bool, withPayDate: Bool) {
//        SPFirebase.Analytics.event("new_incident", parameters: [
//            "type" : type.rawValue,
//            "currency" : currency.code.lowercased(),
//            "note" : withNote ? "set" : "none",
//            "pay_date" : withPayDate ? "set" : "none"
//        ])
//    }
//
//    func send() {
//        switch self {
//        case .lock, .unlock:
//            SPFirebase.Analytics.event("auth", parameters: [
//                "action" : self.id
//            ])
//        case .save_backup, .restore_from_backup:
//            SPFirebase.Analytics.event("backup", parameters: [
//                "action" : self.id
//            ])
//        default:
//            SPFirebase.Analytics.event(self.id, parameters: [:])
//        }
//    }
//}
//
