//
//  Preferens.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 21/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import Foundation


class Preferences {
    
    static var instanse = Preferences()
    
    let userDefaults: UserDefaults
    
    init() {
        userDefaults = UserDefaults.standard
    }
    
    static func getInstans()-> Preferences{
        return instanse
    }
    
    static func removeUserDefaults(){
       UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
    }
    
    static func isNeedAuth()->Bool{
        return getInstans().getSessionToken().isEmpty
    }
    
    func setLicense(license:String)
    {
        userDefaults.setValue(license, forKey: "license")
    }
    
    func getLicense()-> String
    {
        return (userDefaults.string(forKey: "license") ?? "")!
    }
    
    func setSessionToken(sessionToken:String){
        userDefaults.setValue(sessionToken, forKey: "session_token")
    }
    
    func getSessionToken() -> String{
        return (userDefaults.string(forKey: "session_token") ?? "")!
    }
    
    func setSessionTimeLeft(sessionTimeLeft: Int){
        userDefaults.setValue(sessionTimeLeft, forKey: "session_time_left")
    }
    
    func getSessionTimeLeft() -> Int {
        return userDefaults.integer(forKey: "session_time_left")
    }
    
    func setUserName(userName:String){
        userDefaults.setValue(userName, forKey: "user_name")
    }
    
    func getUserName() -> String{
        return (userDefaults.string(forKey: "userName") ?? "")!
    }
    
    func setPhoneNumber(phoneNumber: String){
        userDefaults.setValue(phoneNumber, forKey: "phone_number")
    }
    
    func getPhoneNumber() -> String {
        return (userDefaults.string(forKey: "phone_number") ?? "")!
    }
    
    func setIncidentHash(incidentHash: Int){
        userDefaults.setValue(incidentHash, forKey: "incident_hash")
    }
    
    func getIncidentHash() -> Int {
        return userDefaults.integer(forKey: "incident_hash")
    }
}
