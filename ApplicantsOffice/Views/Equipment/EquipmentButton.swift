//
//  EquipmentButton.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 31/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit

class EquipmentButton: SPButton {
    
    let userNameLabel = SPLabel()
    let commentLabel = UILabel()
    let placeLabel = UILabel()
    let arrowView = ArrowView()
    
    var horizontalInset: CGFloat = Space.side
    var verticalInset: CGFloat = Space.side
    var spaceTitles: CGFloat = 2
    
    override var isHighlighted: Bool {
        didSet {
            if isHighlighted{
                UIView.animate(withDuration: 0.1, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 1.0, options: [.curveEaseOut, .beginFromCurrentState], animations: {
                    self.userNameLabel.alpha = 0.45
                    self.arrowView.alpha = 0.45
                    self.commentLabel.alpha = 0.35
                    self.placeLabel.alpha = 0.35
                }, completion: nil)
            } else {
                UIView.animate(withDuration: 0.35, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 1.0, options: [.curveEaseOut, .beginFromCurrentState], animations: {
                    self.userNameLabel.alpha = 1
                    self.arrowView.alpha = 1
                    self.commentLabel.alpha = 0.5
                    self.placeLabel.alpha = 0.5
                }, completion: nil)
            }
        }
    }
    
    override func commonInit() {
        super.commonInit()
        
//        self.addSubview(self.userImageView)
        
        self.userNameLabel.textAlignment = .left
        self.userNameLabel.font = UIFont.system(weight: .medium, size: 17)
        self.userNameLabel.textColor = Color.Assest.text
        self.userNameLabel.sizeToFit()
        self.addSubview(self.userNameLabel)
        
        self.commentLabel.textAlignment = .left
        self.commentLabel.font = UIFont.system(weight: .regular, size: 11)
        self.commentLabel.textColor = Color.Assest.text.withAlphaComponent(0.5)
        self.commentLabel.sizeToFit()
        self.addSubview(self.commentLabel)
        
        self.placeLabel.textAlignment = .left
        self.placeLabel.font = UIFont.system(weight: .regular, size: 11)
        self.placeLabel.textColor = Color.Assest.text.withAlphaComponent(0.5)
        self.placeLabel.sizeToFit()
        self.addSubview(self.placeLabel)
        
        
        self.addSubview(self.arrowView)
        
        self.frame.set(height: 87)
        self.backgroundColor = Color.Assest.area
        self.layer.cornerRadius = Style.cornerRadius
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
//        let imageSideSize: CGFloat = self.frame.height -  (self.verticalInset * 2)
//        self.userImageView.frame = CGRect.init(x: self.horizontalInset, y: 0, width: imageSideSize, height: imageSideSize)
//        self.userImageView.setYCenter()
        
        self.arrowView.setYCenter()
        self.arrowView.frame.bottomX = self.frame.width - self.horizontalInset
        
        self.userNameLabel.sizeToFit()
        
        self.commentLabel.sizeToFit()
        self.placeLabel.sizeToFit()
        
        let allHeight = self.userNameLabel.frame.height + self.commentLabel.frame.height + self.placeLabel.frame.height + self.spaceTitles
        
        self.userNameLabel.frame.origin.x = Space.modalSide
//        self.userImageView.frame.bottomX + 17
        self.userNameLabel.frame.origin.y = (self.frame.height - allHeight) / 2
        self.userNameLabel.frame.set(width: self.frame.width - self.userNameLabel.frame.origin.x - 10 - (self.frame.width - self.arrowView.frame.origin.x))
        
        self.commentLabel.frame.origin.x = self.userNameLabel.frame.origin.x
        self.commentLabel.frame.origin.y = self.userNameLabel.frame.bottomY + self.spaceTitles
        self.commentLabel.frame.set(width: self.userNameLabel.frame.width)
        
        self.placeLabel.frame.origin.x = self.commentLabel.frame.origin.x
        self.placeLabel.frame.origin.y = self.commentLabel.frame.bottomY + self.spaceTitles
        self.placeLabel.frame.set(width: self.commentLabel.frame.width)
        
    }
}
