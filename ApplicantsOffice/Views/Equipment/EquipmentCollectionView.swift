//
//  EquipmentCollectionView.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 20/11/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit

class EquipmentCollectionView: SPPageCollectionView {
    
    private let identificator: String = "SPEquipmentCollectionViewCell"
    
    override func commonInit() {
        super.commonInit()
        self.layout.scrollDirection = .vertical
        self.showsVerticalScrollIndicator = true
        self.contentInset.top = Space.vertical / 2
        self.contentInset.bottom = Space.vertical
        self.layout.heightFactor = 1
        self.layout.widthFactor = 1
        self.layout.maxItemSpace = Space.side / 2
        self.layout.minItemSpace = Space.side / 2
        self.layout.maxHeight = 87
        self.backgroundColor = UIColor.clear
        self.register(EquipmentCollectionViewCell.self, forCellWithReuseIdentifier: self.identificator)
    }
    
    func dequeueCell(indexPath: IndexPath) -> EquipmentCollectionViewCell {
        return self.dequeueReusableCell(withReuseIdentifier: self.identificator, for: indexPath) as! EquipmentCollectionViewCell
    }
}
