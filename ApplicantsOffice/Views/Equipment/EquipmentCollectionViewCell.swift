//
//  EquipmentCollectionViewCell.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 20/11/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit

class EquipmentCollectionViewCell: SPCollectionViewCell {
    
    let gradeView = UIView()
    let equipmentView = EquipmentButton()
    
    var equipment: Equipment? = nil {
        didSet {
            if let equipment = self.equipment {
                self.equipmentView.userNameLabel.text = equipment.equipment_title == " " ? "?" : equipment.equipment_title
//                self.equipmentView.commentLabel.text = Text.tap_to_change
//                self.equipmentView.userImageView.set(user: self.user)
//                self.equipmentView.userImageView.set(user: nil)
            } else {
                self.equipmentView.userNameLabel.text = Text.error
                self.equipmentView.commentLabel.text = ""
                self.equipmentView.placeLabel.text = ""
//                self.equipmentView.userImageView.set(user: nil)
            }
        }
    }
    
    override var isHighlighted: Bool{
        didSet{
            if isHighlighted{
                UIView.animate(withDuration: 0.27, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 1.0, options: [.curveEaseOut, .beginFromCurrentState], animations: {
                    self.gradeView.alpha = 0.1
                    self.transform = self.transform.scaledBy(x: 0.97, y: 0.97)
                }, completion: nil)
            } else {
                UIView.animate(withDuration: 0.27, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 1.0, options: [.curveEaseOut, .beginFromCurrentState], animations: {
                    self.gradeView.alpha = 0
                    self.transform = CGAffineTransform.identity
                }, completion: nil)
            }
        }
    }
    
    override func commonInit() {
        super.commonInit()
        self.gradeView.alpha = 0
        self.gradeView.backgroundColor = UIColor.black
        self.addSubview(self.gradeView)
        SPConstraints.setEqualSizeSuperview(for: gradeView)
        
        self.equipmentView.isUserInteractionEnabled = false
        self.addSubview(self.equipmentView)
        SPConstraints.setEqualSizeSuperview(for: self.equipmentView)
        
        self.backgroundColor = UIColor.clear
        self.layer.masksToBounds = true
        self.layer.cornerRadius = Style.cornerRadius
    }
}

