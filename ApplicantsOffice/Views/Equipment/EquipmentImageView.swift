//
//  EquipmentImageView.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 31/10/2019.
//  Copyright © 2019 example. All rights reserved.
//
import UIKit

class EquipmentImageView: SPDownloadingImageView {

    override init() {
        super.init()
        self.backgroundColor = Color.Assest.text_in_area.withAlphaComponent(0.2)
        self.tintColor = Color.Assest.area
        self.contentMode = .scaleAspectFit
        self.layer.masksToBounds = true
        self.round = true
        self.setImage(image: UIImage.init(named: "Print")!.alwaysTemplate, animatable: false)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(user: Equipment?) {
        self.contentMode = .scaleAspectFit
        self.setImage(image: UIImage.init(named: "Print")!.alwaysTemplate, animatable: false)
    }
}
