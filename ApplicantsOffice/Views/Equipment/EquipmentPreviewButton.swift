//
//  EquipmentPreviewButton.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 31/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit

class EquipmentPreviewButton: SPButton {
    
    let label = UILabel()
    let circeView = SPView()
    let iconView = UIImageView()
    
    override var isHighlighted: Bool {
        didSet{
            if isHighlighted{
                UIView.animate(withDuration: 0.1, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 1.0, options: [.curveEaseOut, .beginFromCurrentState], animations: {
                    for view in [self.label, self.circeView] {
                        view.alpha = 0.5
                    }
                }, completion: nil)
            } else {
                UIView.animate(withDuration: 0.35, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 1.0, options: [.curveEaseOut, .beginFromCurrentState], animations: {
                    for view in [self.label, self.circeView] {
                        view.alpha = 1
                    }
                }, completion: nil)
            }
        }
    }
    
    override func commonInit() {
        super.commonInit()
        
        self.label.isUserInteractionEnabled = false
        self.label.text = "Инцидент #"
        self.label.font = UIFont.system(weight: .demiBold, size: 15)
        self.label.textColor = UIColor.white
        self.label.textAlignment = .left
        self.addSubview(self.label)
        
        self.circeView.backgroundColor = UIColor.white
        self.circeView.isUserInteractionEnabled = false
        self.circeView.round = true
//        self.addSubview(self.circeView)
        
        self.iconView.backgroundColor = UIColor.clear
        self.iconView.isUserInteractionEnabled = false
        self.iconView.image = UIImage.init(named: "Disclosure Indicator Rounded")!.withRenderingMode(.alwaysTemplate)
        self.iconView.tintColor = Color.base
        self.iconView.contentMode = .scaleAspectFit
//        self.circeView.addSubview(self.iconView)
    }
    
    override func sizeToFit() {
        super.sizeToFit()
        self.frame.set(height: 18)
        self.layoutSubviews()
        self.frame.set(width: self.circeView.frame.bottomX)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.label.sizeToFit()
        self.label.frame.origin.x = 0
        self.label.center.y = self.frame.height / 2
        
        self.circeView.frame.set(width: 14, height: 14)
        self.circeView.frame.origin.x = self.label.frame.bottomX + 5
        self.circeView.center.y = self.frame.height / 2
        
        let iconSideSize = self.circeView.frame.height * 0.46
        self.iconView.frame = CGRect.init(x: 0, y: 0, width: iconSideSize, height: iconSideSize)
        self.iconView.center.x = self.circeView.frame.width / 2 + 0.25
        self.iconView.setYCenter()
    }
}
