//
//  GradientView.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 31/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit

class GradientView: SPGradientView {
    
    override func commonInit() {
        super.commonInit()
        self.startColor = Color.gradient.first
        self.endColor = Color.gradient.second
        self.startColorPosition = .topLeft
        self.endColorPosition = .bottomRight
    }
}

