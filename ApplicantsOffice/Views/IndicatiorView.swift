//
//  IndicatiorView.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 20/11/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit

class IndicatorView: UIView {
    
    private let circleView: UIView = UIView()
    let view = UIView()
    let label = UILabel()
    let button = UIButton()
    
    let player = SPAudioPlayer()
    
    init() {
        super.init(frame: .zero)
        
        self.view.backgroundColor = Color.base
        self.view.layer.masksToBounds = true
        self.addSubview(self.view)
        
        self.circleView.backgroundColor = Color.Assest.bakground
        self.view.addSubview(self.circleView)
        
        self.label.font = UIFont.system(weight: .demiBold, size: 17)
        self.label.textAlignment = .left
        self.label.textColor = Color.Assest.text
        self.addSubview(self.label)
        
        self.button.setTitle(Text.scan_again)
        self.button.titleLabel?.font = UIFont.system(weight: .demiBold, size: 17)
        self.button.titleLabel?.textAlignment = .left
        self.button.setTitleColor(Color.base)
        self.button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 5)
        self.button.contentHorizontalAlignment = .left
        self.addSubview(self.button)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.view.frame = CGRect.init(x: 0, y: 0, width: 28, height: 28)
        self.view.center.y = self.frame.height / 2
        self.view.frame.origin.x = 0
        self.view.round()
        
        if circleView.transform == .identity {
            self.circleView.frame = CGRect.init(x: 0, y: 0, width: 16, height: 16)
            self.circleView.center = CGPoint.init(x: self.view.frame.width / 2, y: self.view.frame.height / 2)
            self.circleView.round()
        }
        
        self.label.frame.origin = CGPoint.init(x: self.view.frame.bottomX + Space.side, y: 0)
        self.label.sizeToFit()
        self.label.frame.set(width: self.frame.width - self.label.frame.origin.x)
        
        self.button.frame.origin = CGPoint.init(x: self.view.frame.bottomX + Space.side, y: 0)
        self.button.sizeToFit()
        self.button.frame.set(width: self.frame.width - self.button.frame.origin.x)
        self.button.frame.bottomY = self.frame.height
    }
    
    override func sizeToFit() {
        super.sizeToFit()
        self.frame.set(height: 42)
    }
    
    func set(mode: Mode, animatable: Bool) {
        
        var labelText: String = ""
        var audio: String = "blip1.wav"
        
//        switch mode {
//        case .fine:
//            labelText = Text.finances_are_fine
//            audio = "blip1.wav"
//        case .overdue_one:
//            labelText = Text.there_is_an_overdue_debt
//            audio = "echo_affirm.wav"
//        case .overdue_more:
//            labelText = Text.there_is_an_overdue_debts
//            audio = "echo_affirm.wav"
//        }
        
        if animatable {
            SPAnimation.animateWithRepeatition(0.4, animations: {
                self.circleView.transform = CGAffineTransform.init(scaleX: 0.6, y: 0.6)
            })
//            self.button.setTitle(Text.сhecking)
            
            if Flags.audioEnabled {
                delay(1.4, closure: {
                    self.player.play(fileName: audio)
                })
            }
            
            delay(1.6, closure: {
                SPAnimation.animate(0.3, animations: {
                    self.circleView.transform = CGAffineTransform.identity
                }, options: UIView.AnimationOptions.beginFromCurrentState, withComplection: {
                    
                })
            })
            
            delay(1.2, closure: {
                self.label.text = labelText
            })
            
            delay(1.2, closure: {
                self.button.setAnimatableText(Text.scan_again)
                delay(0.15, closure: {
                    SPVibration.impact(.light)
                })
            })
        } else {
            self.label.text = labelText
            self.button.setTitle(Text.scan_again)
        }
    }
    
    enum Mode {
        case fine
        case overdue_one
        case overdue_more
    }
}
