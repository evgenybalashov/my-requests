//
//  LightGroup.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 06/11/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit

class LightButton: SPNativeLargeButton {
    
    let arrowView = ArrowView()
    
    override var isHighlighted: Bool {
        didSet {
            if isHighlighted{
                UIView.animate(withDuration: 0.1, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 1.0, options: [.curveEaseOut, .beginFromCurrentState], animations: {
                    self.arrowView.alpha = 0.65
                }, completion: nil)
            } else {
                UIView.animate(withDuration: 0.35, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 1.0, options: [.curveEaseOut, .beginFromCurrentState], animations: {
                    self.titleLabel?.alpha = 1
                    self.arrowView.alpha = 1
                }, completion: nil)
            }
        }
    }
    
    override func commonInit() {
        super.commonInit()
        self.arrowView.isHidden = true
        self.addSubview(self.arrowView)
        self.backgroundColor = Color.Assest.area
        self.setTitleColor(Color.Assest.text_in_area)
        self.titleLabel?.font = UIFont.system(weight: .regular, size: 15)
        self.layer.cornerRadius = Style.cornerRadius
        self.frame.set(height: Style.buttonHeight)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.arrowView.setYCenter()
        self.arrowView.frame.bottomX = self.frame.width - self.contentEdgeInsets.left
    }
}
