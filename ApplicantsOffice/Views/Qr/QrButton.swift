import UIKit

class QrButton: SPButton {
    
    override var isHighlighted: Bool{
        didSet {
            if isHighlighted{
                UIView.animate(withDuration: 0.27, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 1.0, options: [.curveEaseOut, .beginFromCurrentState], animations: {
                    self.transform = CGAffineTransform.identity.scaledBy(x: 0.92, y: 0.92)
                }, completion: nil)
            } else {
                UIView.animate(withDuration: 0.27, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 1.0, options: [.curveEaseOut, .beginFromCurrentState], animations: {
                    self.transform = CGAffineTransform.identity
                }, completion: nil)
            }
        }
    }
    
    
    override func commonInit() {
        super.commonInit()
        
        let backgroundImage:UIImage? = UIImage(named: "qr-code-2.png")
        let backgroundImageResize = imageResize(with: backgroundImage!, scaledTo: CGSize(width: 20.0, height: 20.0))
        
        
        if #available(iOS 13.0, *) {
            self.setImage(backgroundImageResize.withTintColor(Color.base))
        } else {
            // Fallback on earlier versions
//            backgroundImageResize.withRenderingMode(.alwaysTemplate).
//               imageView.image = tintableImage
            
//            let tintableImage = backgroundImageResize.withRenderingMode(.alwaysTemplate)
//            imageView!.image = tintableImage
//            imageView?.tintColor = Color.base
            
//            self.setImage()
        }
        
        
        
        self.backgroundColor = Color.Assest.area
        self.setTitleColor(Color.Assest.text_in_area, for: .normal)
        self.titleLabel?.font = UIFont.system(weight: .regular, size: 15)
        self.layer.cornerRadius = Style.cornerRadius
        
        self.frame.set(width: Style.buttonHeight, height: Style.buttonHeight)
    }
    
    
    func imageResize(with image: UIImage, scaledTo newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage ?? UIImage()
    }
    
   
}


