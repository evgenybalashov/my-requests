//
//  TextField.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 31/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit

class TextField: SPTextField {
    
    override func commonInit() {
        super.commonInit()
        self.backgroundColor = Color.Assest.area
        self.font = UIFont.system(weight: .regular, size: 15)
        self.textColor = Color.Assest.text_in_area
        self.textInsets.left = Space.side
        self.textInsets.right = Space.side
        self.cursorColor = Color.base
    }
    
    override func sizeToFit() {
        super.sizeToFit()
        self.frame.set(height: Style.buttonHeight)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = Style.cornerRadius
    }
}
