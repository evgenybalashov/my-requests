//
//  ColorButton.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 31/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit

class ColorButton: SPNativeLargeButton {
    
    override func commonInit() {
        super.commonInit()
        self.titleLabel?.font = UIFont.system(weight: .demiBold, size: 17)
        self.tintColor = UIColor.white
        self.backgroundColor = Color.base
        self.setTitleColor(UIColor.white)
        self.layer.cornerRadius = Style.cornerRadius
    }
}
