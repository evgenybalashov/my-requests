//
//  EmptyPlaceholderView.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 31/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit

class EmptyPlaceholerView: SPView {
    
    let titleLabel = SPLabel()
    let button = SPButton()
    
    var withButton: Bool = true {
        didSet {
            self.button.isHidden = !self.withButton
            self.layoutSubviews()
        }
    }
    
    override func commonInit() {
        super.commonInit()
        self.backgroundColor = Color.Assest.area
        self.layer.cornerRadius = Style.cornerRadius
        
        self.titleLabel.textColor = Color.Assest.text_in_area
        self.titleLabel.setCenterAlignment()
        self.titleLabel.font = UIFont.system(weight: .regular, size: 15)
        self.titleLabel.text = "Text.no_active_debt"
        self.addSubview(self.titleLabel)
        
        self.button.setTitle("Text.add_example")
        self.button.titleLabel?.font = UIFont.system(weight: .regular, size: 15)
        self.button.setTitleColor(Color.base)
        self.addSubview(self.button)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.titleLabel.sizeToFit()
        self.titleLabel.setXCenter()
        self.button.sizeToFit()
        self.button.setXCenter()
        
        let space: CGFloat = 7
        let allHeight: CGFloat = self.titleLabel.frame.height + space + self.button.frame.height
        self.titleLabel.frame.origin.y = (self.frame.height - allHeight) / 2
        self.button.frame.origin.y = self.titleLabel.frame.bottomY + space
        
        if !self.withButton {
            self.titleLabel.center.y = self.frame.height / 2
        }
        
    }
}

