//
//  IncidentCreateButton.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 31/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit

class IncidentButton: SPAppleMusicButton {
    
    override func commonInit() {
        super.commonInit()
        self.selectColor = Color.base
        self.baseColor = Color.Assest.area
    }
}
