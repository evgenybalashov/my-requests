//
//  IncidentTableViewCell.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 28/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit
import Crashlytics

class IncidentTableViewCell: UITableViewCell {

   static let reuseIdentifier = "incidentCell"

      @IBOutlet var incident_id: UILabel!
      @IBOutlet var timestamp_create: UILabel!
      @IBOutlet var equipment_title: UILabel!
      @IBOutlet var status_title: UILabel!

}
