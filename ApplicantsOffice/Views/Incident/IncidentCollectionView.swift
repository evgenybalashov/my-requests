//
//  IncidentCollectionView.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 31/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit

class IncidentCollectionView: SPPageCollectionView {
    
    let emptyPlaceholderView = EmptyPlaceholerView()
    private let incidentIdentificator: String = "SPProposeCollectionViewCell"
    
    override func commonInit() {
        print("init collection view")
        super.commonInit()
        self.layout.scrollDirection = .horizontal
        self.layout.heightFactor = 0.72
        self.layout.widthFactor = 1
        //self.layout.maxWidth = UIScreen.main.bounds.width * 0.86
        self.layout.cellSideRatio = 316 / 198
        self.layout.maxItemSpace = Space.side / 1.25
        self.layout.minItemSpace = Space.side / 1.25
        self.backgroundColor = UIColor.clear
        self.register(IncidentCollectionViewCell.self, forCellWithReuseIdentifier: self.incidentIdentificator)
        
        self.emptyPlaceholderView.isHidden = true
        self.emptyPlaceholderView.alpha = 0
        self.addSubview(self.emptyPlaceholderView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.emptyPlaceholderView.frame = CGRect.init(
            x: 0, y: 0,
            width: self.frame.width - self.contentInset.left * 2,
            height: self.frame.height * self.layout.heightFactor
        )
        self.emptyPlaceholderView.frame.origin.x = 0
        self.emptyPlaceholderView.center.y = self.frame.height / 2
    }
    
    func dequeueIncidentCell(indexPath: IndexPath) -> IncidentCollectionViewCell {
        return self.dequeueReusableCell(withReuseIdentifier: self.incidentIdentificator, for: indexPath) as! IncidentCollectionViewCell
    }
    
    override func reloadData() {
        super.reloadData()
        if self.numberOfItems(inSection: 0) == 0 {
            self.showEmptyPropose(animated: false)
        } else {
            self.emptyPlaceholderView.isHidden = true
            self.emptyPlaceholderView.alpha = 0
        }
    }
    
    private var showing: Bool = false
    
    func showEmptyPropose(animated: Bool) {
        if !self.showing {
            if emptyPlaceholderView.isHidden {
                self.showing = true
                self.emptyPlaceholderView.isHidden = false
                self.emptyPlaceholderView.alpha = 0
                
                let show = {  self.emptyPlaceholderView.alpha = 1 }
                
                if animated {
                    SPAnimation.animate(0.2, animations: {
                        show()
                    }) {
                        self.showing = false
                    }
                } else {
                    show()
                    self.showing = false
                }
            }
        }
    }
    
    func hideEmptyPropose(animated: Bool) {
        SPAnimation.animate(0.2, animations: {
            self.emptyPlaceholderView.alpha = 0
        }) {
            self.emptyPlaceholderView.isHidden = true
            self.showing = false
        }
    }
}

