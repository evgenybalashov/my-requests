//
//  IncidentCollectionViewCell.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 31/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit

class IncidentCollectionViewCell: SPCollectionViewCell {
    
    var mode: Mode = .base {
        didSet {
            switch self.mode {
            case .base:
//                self.userButton.isHidden = false
//                self.dotsButton.isHidden = false
//                self.createDateLabel.isHidden = Flags.isShowCommentOnCell
//                self.noteLabel.isHidden = !self.createDateLabel.isHidden
                break
            case .contact:
//                self.userButton.isHidden = true
//                self.dotsButton.isHidden = true
//                self.timeCreated.isHidden = false
//                self.createDateLabel.isHidden = false
//                self.noteLabel.isHidden = true
                break
            }
            
            self.layoutSubviews()
        }
    }
    
    let commentLabel = UILabel()
    let label = UILabel()
    
    let createDateLabel = UILabel()
    let actionDateLabel = UILabel()
    let noteLabel = UILabel()
    
    let userButton = EquipmentPreviewButton()
//    let dotsButton = SPDotButton()
    let timeCreated = UILabel()
    
    let gradeView = UIView()
    let gradientView = GradientView()
    
    var customShadowOpacity: Float = 0.4 {
        didSet {
            self.layoutSubviews()
        }
    }
    
    override var isHighlighted: Bool{
        didSet{
            if isHighlighted {
                UIView.animate(withDuration: 0.27, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 1.0, options: [.curveEaseOut, .beginFromCurrentState], animations: {
                    self.gradeView.alpha = 0.15
                    self.transform = self.transform.scaledBy(x: 0.94, y: 0.94)
                }, completion: nil)
            } else {
                UIView.animate(withDuration: 0.27, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 1.0, options: [.curveEaseOut, .beginFromCurrentState], animations: {
                    self.gradeView.alpha = 0
                    self.transform = CGAffineTransform.identity
                }, completion: nil)
            }
        }
    }
    
    override func commonInit() {
        super.commonInit()
        self.addSubview(self.gradientView)
        
        self.gradeView.alpha = 0
        self.gradeView.backgroundColor = UIColor.init(hex: "8C1028")
        self.addSubview(self.gradeView)
        
        self.commentLabel.isUserInteractionEnabled = false
        self.commentLabel.text = "Оборудование:"
        self.commentLabel.textColor = UIColor.white
        self.commentLabel.font = UIFont.system(weight: .regular, size: 21)
        self.commentLabel.setCenterAlignment()
        self.addSubview(self.commentLabel)
        
        self.label.isUserInteractionEnabled = false
        self.label.text = "$750"
        self.label.textColor = UIColor.white
        self.label.font = UIFont.system(weight: .medium, size: 14)
        self.label.setCenterAlignment()
        self.addSubview(self.label)
        
        self.createDateLabel.isUserInteractionEnabled = false
        self.createDateLabel.text = "Статус"
        self.createDateLabel.textColor = UIColor.white
        self.createDateLabel.font = UIFont.system(weight: .regular, size: 15)
        self.createDateLabel.textAlignment = .left
        self.addSubview(self.createDateLabel)
//        self.createDateLabel.isHidden = Flags.isShowCommentOnCell
        
        self.actionDateLabel.isUserInteractionEnabled = false
        self.actionDateLabel.text = ""
        self.actionDateLabel.textColor = UIColor.yellow
        self.actionDateLabel.font = UIFont.system(weight: .regular, size: 18)
        self.actionDateLabel.textAlignment = .right
        self.addSubview(self.actionDateLabel)
        
        self.noteLabel.isUserInteractionEnabled = false
        self.noteLabel.numberOfLines = 2
        self.noteLabel.text = ""
        self.noteLabel.textColor = UIColor.white
        self.noteLabel.font = UIFont.system(weight: .regular, size: 13)
        self.noteLabel.textAlignment = .left
        self.addSubview(self.noteLabel)
//        self.noteLabel.isHidden = !Flags.isShowCommentOnCell
        
        self.addSubview(self.userButton)
        
        self.timeCreated.isUserInteractionEnabled = false
        self.timeCreated.text = "Закрыт"
       self.timeCreated.textColor = UIColor.white
       self.timeCreated.font = UIFont.system(weight: .regular, size: 13)
       self.timeCreated.textAlignment = .left
       self.addSubview(self.timeCreated)

        
//        self.dotsButton.backgroundColor = UIColor.black.withAlphaComponent(0.24)
//        self.dotsButton.customSideSize = 28
//        self.addSubview(self.dotsButton)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if self.isHighlighted { return }
        
        self.gradientView.setSuperviewBounds()
        self.gradeView.setSuperviewBounds()
        self.layer.cornerRadius = Style.cornerRadius * 1.35
        self.gradeView.layer.cornerRadius = self.layer.cornerRadius
        self.gradientView.layer.cornerRadius = self.layer.cornerRadius
        self.gradientView.gradientLayer.cornerRadius = self.layer.cornerRadius
        self.updateShadow()
        
        let horizontalSideSpace: CGFloat = 18
        let verticalSideSpace: CGFloat = 16
        
        switch self.mode {
        case .base:
            self.commentLabel.sizeToFit()
            self.label.sizeToFit()
            self.commentLabel.setXCenter()
            self.commentLabel.frame.origin.y = (self.frame.height - (self.commentLabel.frame.height + self.label.frame.height)) / 2
            self.label.frame.origin.y = self.commentLabel.frame.bottomY
            self.label.setXCenter()
            
            self.createDateLabel.sizeToFit()
            self.createDateLabel.frame.origin.x = horizontalSideSpace
            self.createDateLabel.frame.bottomY = self.frame.height - verticalSideSpace
            
            self.noteLabel.frame.set(width: self.frame.width * 0.45)
            self.noteLabel.sizeToFit()
            self.noteLabel.frame.origin.x = horizontalSideSpace
            self.noteLabel.frame.bottomY = self.frame.height - verticalSideSpace
            
            self.actionDateLabel.sizeToFit()
            self.actionDateLabel.frame.bottomX = self.frame.width - horizontalSideSpace
            self.actionDateLabel.frame.bottomY = self.frame.height - verticalSideSpace
            
            self.timeCreated.sizeToFit()
            self.timeCreated.frame.bottomX = self.frame.width - horizontalSideSpace
            self.timeCreated.frame.origin.y = verticalSideSpace
//            self.dotsButton.sizeToFit()
//            self.dotsButton.frame.bottomX = self.frame.width - horizontalSideSpace
//            self.dotsButton.frame.origin.y = verticalSideSpace
            
            self.userButton.sizeToFit()
            self.userButton.frame.origin.x = horizontalSideSpace
            self.userButton.frame.origin.y = verticalSideSpace
            break
        case .contact:
            self.commentLabel.sizeToFit()
            self.commentLabel.frame.origin.y = verticalSideSpace
            self.commentLabel.frame.origin.x = horizontalSideSpace
            
            self.label.sizeToFit()
            self.label.frame.origin.y = verticalSideSpace
            self.label.frame.bottomX = self.frame.width - horizontalSideSpace
            
            self.actionDateLabel.sizeToFit()
            self.actionDateLabel.frame.bottomX = self.frame.width - horizontalSideSpace
            self.actionDateLabel.frame.bottomY = self.frame.height - verticalSideSpace
            break
        }
    }
    
    func updateShadow() {
        let shadowWidth = self.frame.width * 0.77
        let shadowHeight = self.frame.height * 0.5
        
        let cornerRadius = self.layer.cornerRadius
        
        let xTranslate = (self.frame.width - shadowWidth) / 2
        let yTranslate = (self.frame.height - shadowHeight) + 4
        
        let shadowPath = UIBezierPath.init(
            roundedRect: CGRect.init(x: xTranslate, y: yTranslate, width: shadowWidth, height: shadowHeight),
            cornerRadius: cornerRadius
        )
        
        self.layer.shadowColor = Color.base.cgColor
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowOpacity = self.customShadowOpacity
        self.layer.shadowRadius = 17.55
        self.layer.masksToBounds = false
        self.layer.shadowPath = shadowPath.cgPath
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.mode = .base
        self.userButton.removeAllTargets()
//        self.dotsButton.removeAllTargets()
        self.customShadowOpacity = 0.4
    }
    
    func set(incident: Incident, controller: Controller){
        let currencySymbolCount = 3
//        debt.currency.symbol.count
//        if currencySymbolCount < 2 {
        self.label.text = "УИН: " + (incident.equipment_uin ?? "")
//        "HP Color LaserJet Pro M477fdn"
        self.commentLabel.text = incident.equipment_title
//                debt.formattedAmount
//        } else {

            let formatter = NumberFormatter()
            formatter.numberStyle = .currency
            formatter.maximumFractionDigits = 0
            formatter.currencySymbol = ""
//
            let amountString = formatter.string(for: Int("13")) ?? "?"
//
            let text = "\(amountString)"
//            self.label.setMultiSize(text, width: .bold, positions: [UILabel.FormatPosition(start: text.count - currencySymbolCount, length: currencySymbolCount)], base: 35, higlighted: 18, higlightedColor: UIColor.white.withAlphaComponent(0.7))
//        }

//        self.commentLabel.text = debt.type.describtion
        self.userButton.label.text = "Инцидент # " + String(incident.incident_id)
//        self.dotsButton.target { controller.showActions(for: debt, sourceView: self.dotsButton) }
//        self.userButton.target { controller.show(user: debt.user) }
//
//        let createDateTitle = debt.createDate.format(mask: "dd, MMMM")
//        self.createDateLabel.text = "\(Text.created): " + createDateTitle
//
//        if Calendar.current.isDateInToday(debt.createDate) {
//            self.createDateLabel.text = Text.created_today
//        }
//
//        let actionDateFormatTitle = debt.actionDate?.format(mask: "dd, MMMM") ?? ""
//        let actionTitle: String = debt.type == .create ? Text.pay_vernet : Text.pay_vernut
//        let actionDateTitle: String = { () -> String in
//            if actionDateFormatTitle == "" {
//                return ""
//            } else {
//                return actionTitle + ": " + actionDateFormatTitle
//            }
//        }()
        self.timeCreated.text = "Создано: " + DateTimeHelper.getDateFromTimeStamp(timeStamp: Double(incident.timestamp_create))
        
        self.createDateLabel.text = "Статус: " + incident.status_title
        
        if (incident.in_work == false){
            let starsWhite = "\u{2606}"
            let starsYellow = "\u{2605}"
            switch incident.rating{
            case 1: return self.actionDateLabel.text =
                starsYellow+starsWhite+starsWhite+starsWhite+starsWhite
            case 2: return self.actionDateLabel.text = starsYellow+starsYellow+starsWhite+starsWhite+starsWhite
            case 3: return self.actionDateLabel.text =
                starsYellow+starsYellow+starsYellow+starsWhite+starsWhite
            case 4: return self.actionDateLabel.text =
                starsYellow+starsYellow+starsYellow+starsYellow+starsWhite
            case 5: return self.actionDateLabel.text =
                starsYellow+starsYellow+starsYellow+starsYellow+starsYellow
            default:
                return self.actionDateLabel.text = ""
            }
        }
//        self.actionDateLabel.text = "sdasdasdas"
//        self.noteLabel.text = ""
    }
    
    enum Mode {
        case base
        case contact
    }
}
