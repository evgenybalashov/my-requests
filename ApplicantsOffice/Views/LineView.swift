//
//  File.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 31/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit

class LineView: SPView {
    
    override func commonInit() {
        super.commonInit()
        self.backgroundColor = Color.Assest.text_in_area.withAlphaComponent(0.4)
        self.frame.set(height: 0.4)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.round()
    }
}
