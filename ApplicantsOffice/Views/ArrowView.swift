//
//  ArrowView.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 31/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit

class ArrowView: SPImageView {
    
    override func commonInit() {
        super.commonInit()
        self.isUserInteractionEnabled = false
        self.image = UIImage.init(named: "Disclosure Indicator")!.withRenderingMode(.alwaysTemplate)
        self.tintColor = Color.Assest.text_in_area.withAlphaComponent(0.5)
        self.contentMode = .scaleAspectFit
        self.frame.set(height: 13)
        self.frame.set(width: 8)
    }
}
