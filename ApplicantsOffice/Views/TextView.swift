//
//  TextView.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 31/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit

class TextView: SPTextView {
    
    override func commonInit() {
        super.commonInit()
        self.backgroundColor = Color.Assest.area
        self.font = UIFont.system(weight: .regular, size: 15)
        self.textColor = Color.Assest.text_in_area
        self.textContainerInset.left = Space.side
        self.textContainerInset.right = Space.side
        self.textContainerInset.top = Space.side
        self.textContainerInset.bottom = Space.side
        self.showsVerticalScrollIndicator = false
        self.showsHorizontalScrollIndicator = false
        self.cursorColor = Color.base
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = Style.cornerRadius
    }
}

