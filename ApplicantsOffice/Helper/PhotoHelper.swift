//
//  PhotoHelper.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 13/11/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit

class PhotoHelper{
    
    static func convertImageToBase64(image: UIImage) -> String {
               let imageData = image.jpegData(compressionQuality: 0.25)!
               return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
}
