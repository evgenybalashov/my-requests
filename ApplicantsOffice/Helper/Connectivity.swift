//
//  Connectivity.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 14/11/2019.
//  Copyright © 2019 example. All rights reserved.
//

import Foundation
import SystemConfiguration
import Alamofire

class Connectivity {
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}
