//
//  FileHelper.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 22/11/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit
import Foundation

class FileHelper{
    
    //получить путь к каталогу документа
    static func getDirectoryPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    //создать каталог
    static func createDirectory(){
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("mkz")
        if !fileManager.fileExists(atPath: paths){
            do {
                try fileManager.createDirectory(atPath: paths, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print("Couldn't create document directory")
            }
        }else{
            print("Already directory created.")
        }
    }
    
    //сохранить изображение в каталоге документов
    static func saveImageToDocumentDirectory(imageName: String, image: UIImage) {
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageName)
//        print("saveImageToDocumentDirectory-path", paths)
//        let image = imageView.image!
//        print("saveImageToDocumentDirectory-image", image)
        let imageData = image.jpegData(compressionQuality: 0.25)
        fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
    }
    
    //Чтение / получение изображения из каталога документов
    static func getImage(imageName : String)-> UIImage{
        let fileManager = FileManager.default
        // Here using getDirectoryPath method to get the Directory path
        let imagePath = (self.getDirectoryPath() as NSString).appendingPathComponent(imageName)
        if fileManager.fileExists(atPath: imagePath){
            return UIImage(contentsOfFile: imagePath)!
        }else{
            return UIImage()
        }
    }
    
    //получение изображения
    static func getImageCount(imageName : String)-> Bool{
        let fileManager = FileManager.default
        // Here using getDirectoryPath method to get the Directory path
        let imagePath = (self.getDirectoryPath() as NSString).appendingPathComponent(imageName)
        if fileManager.fileExists(atPath: imagePath){
            return true
        }else{
            return false
        }
    }
    
    
    //удаление изображения
    static func deleteSaveImage(imageName: String) {
        let fileManager = FileManager.default
        let imagePath = (self.getDirectoryPath() as NSString).appendingPathComponent(imageName)
        print("saveImageToDocumentDirectory-path", imagePath)
        do {
            try fileManager.removeItem(atPath: imagePath)
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
    }
    
    //Удалить каталог по имени
    static func deleteDirectory(directoryName : String){
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(directoryName)
        if fileManager.fileExists(atPath: paths){
            try! fileManager.removeItem(atPath: paths)
        }else{
            print("Directory not found")
        }
    }
    
    //уменьшение размера image
    static func resize(_ image: UIImage) -> UIImage {
        let compressionQuality: Float = 0.25
        let imageData = image.jpegData(compressionQuality: CGFloat(compressionQuality))
        UIGraphicsEndImageContext()
        return UIImage(data: imageData!) ?? UIImage()
    }
    
    
   static func imageIsNullOrNot(imageName : UIImage)-> Bool
    {
        let size = CGSize(width: 0, height: 0)
        if (imageName.size.width == size.width)
        {
            return false
        }
        else
        {
            return true
        }
    }
}
