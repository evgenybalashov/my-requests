//
//  DateTimeHelper.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 24/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import Foundation
import UIKit

class DateTimeHelper {
    
    static func convertDateFormatter(date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"//this your string date format
        let date = dateFormatter.date(from: date)
        
        dateFormatter.dateFormat = "dd/MMM/yyyy"
        let timeStamp = dateFormatter.string(from: date!)
        
        return timeStamp
    }
    
    static func getDateFromTimeStamp(timeStamp : Double) -> String {
           
           let date = NSDate(timeIntervalSince1970: timeStamp)
           
           let dayTimePeriodFormatter = DateFormatter()
           dayTimePeriodFormatter.dateFormat = "dd MMM yyyy"
        
           let dateString = dayTimePeriodFormatter.string(from: date as Date)
           return dateString
       }
}
