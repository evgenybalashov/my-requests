//
//  MyCodeViewController.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 11/11/2019.
//  Copyright © 2019 example. All rights reserved.
//

import Foundation
import swiftScan

class MyCodeViewController: UIViewController {
    
    var qrView = UIView()
    var qrImgView = UIImageView()
    
    var tView = UIView()
    var tImgView = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.white
        
        drawCodeShowView()
        
    }
    
    func drawCodeShowView() {
        
        let rect = CGRect(x: (self.view.frame.width-self.view.frame.width*5/6)/2, y: 100, width: self.view.frame.width*5/6, height: self.view.frame.width*5/6)
        qrView.frame = rect
        self.view.addSubview(qrView)
        
        qrView.backgroundColor = UIColor.white
        qrView.layer.shadowOffset = CGSize(width: 0, height: 2)
        qrView.layer.shadowRadius = 2
        qrView.layer.shadowColor = UIColor.black.cgColor
        qrView.layer.shadowOpacity = 0.5
        
        qrImgView.bounds = CGRect(x: 0, y: 0, width: qrView.frame.width-12, height: qrView.frame.width-12)
        qrImgView.center = CGPoint(x: qrView.frame.width/2, y: qrView.frame.height/2)
        qrView .addSubview(qrImgView)
        
        tView.frame = CGRect(x: (self.view.frame.width-self.view.frame.width*5/6)/2,
                             y: rect.maxY+20,
                             width: self.view.frame.width*5/6,
                             height: self.view.frame.width*5/6*0.5)
        self.view .addSubview(tView)
        tView.layer.shadowOffset = CGSize(width: 0, height: 2)
        tView.layer.shadowRadius = 2
        tView.layer.shadowColor = UIColor.black.cgColor
        tView.layer.shadowOpacity = 0.5
        
        tImgView.bounds = CGRect(x: 0, y: 0, width: tView.frame.width-12, height: tView.frame.height-12)
        tImgView.center = CGPoint(x: tView.frame.width/2, y: tView.frame.height/2)
        tView .addSubview(tImgView)
        
    }
    
    deinit {
        print("MyCodeViewController deinit")
    }
    
}

