//
//  CodeController.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 08/11/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit
import swiftScan
import SPFakeBar

class QQScanViewController: LBXScanViewController {
    
    let navBar = SPFakeBarView(style: .stork)
    
    var topTitle: UILabel?
    
    var isOpenedFlash: Bool = false
    
    var bottomItemsView: UIView?
    
    var btnPhoto: UIButton = UIButton()
    
    var btnFlash: UIButton = UIButton()
    
    var btnMyQR: UIButton = UIButton()
    
    //    var delegate: QRRectDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navBar.titleLabel.text = "Сканирование QR кода"
        self.navBar.separatorView.backgroundColor = Color.Assest.separator
        self.view.addSubview(self.navBar)
        
        setNeedCodeImage(needCodeImg: true)
        
        scanStyle?.centerUpOffset += 10
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        drawBottomItems()
    }
    
    override func handleCodeResult(arrayResult: [LBXScanResult]) {
        
        for result: LBXScanResult in arrayResult {
            if let str = result.strScanned {
                let pasterboard = UIPasteboard.general
                pasterboard.string = str
                //передаем значение
                self.delegate?.uin(uin: str)
            }
        }
        self.dismiss()
    }
    
    func drawBottomItems() {
        if (bottomItemsView != nil) {
            
            return
        }
        
        let yMax = self.view.frame.maxY - self.view.frame.minY
        
        bottomItemsView = UIView(frame: CGRect(x: 0.0, y: yMax-100, width: self.view.frame.size.width, height: 100 ) )
        
        bottomItemsView!.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.6)
        
        self.view .addSubview(bottomItemsView!)
        
        let size = CGSize(width: 65, height: 87)
        
        self.btnFlash = UIButton()
        btnFlash.bounds = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        btnFlash.center = CGPoint(x: bottomItemsView!.frame.width/2, y: bottomItemsView!.frame.height/2)
        
        btnFlash.setImage(UIImage(named: "CodeScan.bundle/qrcode_scan_btn_flash_nor"), for:UIControl.State.normal)
        btnFlash.addTarget(self, action: #selector(QQScanViewController.openOrCloseFlash), for: UIControl.Event.touchUpInside)
        
        
        bottomItemsView?.addSubview(btnFlash)
        
        self.view .addSubview(bottomItemsView!)
        
    }
    
    @objc func openLocalPhotoAlbum()
    {
        let alertController = UIAlertController(title: "title", message:"使用首页功能", preferredStyle: UIAlertController.Style.alert)
        
        let alertAction = UIAlertAction(title:  "知道了", style: UIAlertAction.Style.default) { (alertAction) -> Void in
            
        }
        
        alertController.addAction(alertAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    @objc func openOrCloseFlash() {
        scanObj?.changeTorch()
        
        isOpenedFlash = !isOpenedFlash
        
        if isOpenedFlash
        {
            btnFlash.setImage(UIImage(named: "CodeScan.bundle/qrcode_scan_btn_flash_down"), for:UIControl.State.normal)
        }
        else
        {
            btnFlash.setImage(UIImage(named: "CodeScan.bundle/qrcode_scan_btn_flash_nor"), for:UIControl.State.normal)
            
        }
    }
    
    @objc func myCode() {
        let vc = MyCodeViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

