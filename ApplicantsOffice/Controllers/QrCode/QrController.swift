//
//  QrController.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 07/11/2019.
//  Copyright © 2019 example. All rights reserved.
//

import Foundation
import swiftScan
import SPFakeBar
import SPAlert
import SPStorkController

class QrController: Controller, LBXScanViewControllerDelegate{
    
    let navBar = SPFakeBarView(style: .stork)
    
    override func viewDidLoad() {
           super.viewDidLoad()
        
        self.modalPresentationCapturesStatusBarAppearance = true
        self.view.backgroundColor = Color.Assest.bakground
        
           self.qqStyle()
       }
       
       func qqStyle() {
           print("qqStyle")
           
           let vc = QQScanViewController()
           var style = LBXScanViewStyle()
           style.animationImage = UIImage(named: "CodeScan.bundle/qrcode_scan_light_green")
           vc.scanStyle = style
           self.navigationController?.pushViewController(vc, animated: true)
           
       }
       
       func scanFinished(scanResult: LBXScanResult, error: String?) {
           NSLog("scanResult:\(scanResult)")
       }
       
}
