//
//  StarRated.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 20/11/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit
import Cosmos
import Alamofire


class PopStarRatedViewController: Controller{
    //MARK: Переделать все
    var incident_id: Int = 0
       
       
    func setIncident(incident_id: Int){
        self.incident_id = incident_id
    }
    
    
    @IBOutlet weak var ratedView: CosmosView!
    @IBOutlet weak var commentCustomer: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (UIDevice.modelName == "iPhone SE" || UIDevice.modelName == "iPhone 5" || UIDevice.modelName ==  "iPhone 5c" || UIDevice.modelName == "iPhone 5s" || UIDevice.modelName == "Simulator iPhone SE") || UIDevice.modelName == "iPhone 7" {
            print("ok")
            ratedView.settings.starSize = 50
        }
        
        ratedView.settings.fillMode = .full
        commentCustomer.isHidden = true
        // Do any additional setup after loading the view, typically from a nib.
        
        ratedView.didFinishTouchingCosmos = didFinishTouchingCosmos
        ratedView.didTouchCosmos = didTouchCosmos
    }
    
    @IBAction func cancelRated(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveRated(_ sender: Any) {
        didFinishTouchingCosmos(ratedView.rating)
        redirectToIncident()
//        dismiss(animated: true, completion: nil)
    }

    // Вызывается, когда пользователь заканчивает изменение рейтинга, отрывая палец от обзора.
    // Это может быть хорошим местом для сохранения рейтинга в базе данных или отправки на сервер.
    private func didFinishTouchingCosmos(_ rating: Double) {
        
        let parameters: Parameters = [
            "incident_id": Int(incident_id),
            "rating":  Int(ratedView.rating),
            "comment": commentCustomer.text
        ]
        
        Api.setIncidentRating(parameters: parameters) { (error: Error?, success: String?) in
            if let success = success{
              print("success", success)
                let dict: [String: Any] = ["rating": self.ratedView.rating,
                                           "rating_comment": self.commentCustomer.text]
                Storage.update(incident_id: self.incident_id, with: dict)
                
            }
            
        }
    }
    
    // Закрытие, которое вызывается, когда пользователь изменяет рейтинг, касаясь представления.
    // Это можно использовать для обновления пользовательского интерфейса при изменении рейтинга, перемещая палец.
    private func didTouchCosmos(_ rating: Double){
        if rating <= 3{
            commentCustomer.isHidden = false
            commentCustomer.becomeFirstResponder()
        }else{
            commentCustomer.isHidden = true
            commentCustomer.resignFirstResponder()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func redirectToIncident(){
           DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
               let menuTabController = self.storyboard?.instantiateViewController(withIdentifier: "tabBar") as! TabBarController
                   menuTabController.selectedIndex = 2;
                          self.present(menuTabController, animated: false, completion: nil)
                      })
       }

}
