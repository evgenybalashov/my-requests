//
//  Controller.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 31/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit
import SPStorkController
import SPAlert

class Controller: SPController, StorageDelegate {
    
    var contentWidth: CGFloat = 0
    var modalContentWidth: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.statusBarAnimationDuration = 0.6
    }
    
    override func updateLayout(with size: CGSize) {
        super.updateLayout(with: size)
        self.contentWidth = size.width - (Space.side * 2)
        self.modalContentWidth = size.width - (Space.modalSide * 2)
    }
    
    func show(incident: Incident?) {
        let controller = IncidentController(incident: incident)
        controller.delegte = self
        
        if Flags.useStorkPresentation {
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.showIndicator = true
            transitionDelegate.indicatorColor = Color.Assest.indicator
            transitionDelegate.hideIndicatorWhenScroll = false
            transitionDelegate.showCloseButton = false
            transitionDelegate.confirmDelegate = controller as? SPStorkControllerConfirmDelegate
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            controller.modalPresentationCapturesStatusBarAppearance = true
            self.present(controller)
        } else {
            controller.modalPresentationStyle = .formSheet
            controller.preferredContentSize = CGSize.init(width: 420, height: 742)
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    func showView(incident: Incident?){
        let controller = IncidentViewController(incident: incident)
        controller.delegte = self
        
        if Flags.useStorkPresentation {
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.showIndicator = true
            transitionDelegate.indicatorColor = Color.Assest.indicator
            transitionDelegate.hideIndicatorWhenScroll = false
            transitionDelegate.showCloseButton = false
            transitionDelegate.confirmDelegate = controller as? SPStorkControllerConfirmDelegate
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            controller.modalPresentationCapturesStatusBarAppearance = true
            self.present(controller)
        } else {
            controller.modalPresentationStyle = .formSheet
            controller.preferredContentSize = CGSize.init(width: 420, height: 742)
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    func showEquipments() {
          let controller = EquipmentListController()
          controller.delegate = self
          self.navigationController?.pushViewController(controller, animated: true)
      }
    
    
    func showIncidents() {
        if let incidentController = self as? IncidentsListController {
            let controller = IncidentsListController.init(incidents: incidentController.incidents)
            controller.delegate = self
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func showActions(for incident: Incident){
        self.showView(incident: incident)
    }
    
    func storageUpdated() {
        self.updateLayout(with: self.view.frame.size)
    }
    
}

protocol StorageDelegate: class {
    
    func storageUpdated()
}
