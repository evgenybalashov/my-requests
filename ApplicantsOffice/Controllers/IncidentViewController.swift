//
//  IncidentViewController.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 18/11/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit
import Alamofire
import SPFakeBar
import SPAlert
import SPStorkController
import Photos
import Crashlytics
import RealmSwift
import UserNotifications


class IncidentViewController: Controller {
    
    let navBar = SPFakeBarView(style: .stork)
    
    let amountAndNoteHorizontalScrollView = SPScrollView()
    
    let valueTexts = SPSectionLabelsView()
    //    let qrButton = QrButton()
    
    let uinTextField = TextField()
    let equipmentTitleLabel = SPLabel()
    let equipmentUinLabel = SPLabel()
    let noteLabels = SPSectionLabelsView()
    let noteTextView = TextView()
    
    let lineAfterSummView = LineView()
    let lineAfterButtonsView = LineView()
    
    let photoLabels = SPSectionLabelsView()
    let photoView = UIImageView()
    
    let addButton = ColorButton()
    let blurView = UIView()
    let scrollView = SPScrollView()
    
    var imageView: UIImageView!
    var imagePicker: UIImagePickerController!
    let pickerController = UIImagePickerController()
    let payDateLabels = SPSectionLabelsView()
    weak var delegte: StorageDelegate?
    
    var equipments = [Equipment]()
    
    var uin: String? = nil {
        didSet {
            if let uin = self.uin {
                self.uinTextField.text = uin
            }else{
                self.uinTextField.text = ""
            }
        }
    }
    
    var note: String? = nil {
        didSet {
            if let note = self.note {
                self.noteTextView.text = note
            }else{
                self.noteTextView.text = ""
            }
        }
    }
    
    
    var incident_id: Int? = nil
    var status_title: String? = nil
    var equipment_title: String? = nil
    var equipment_uin: String? = nil
    var timestamp_close: Int? = nil
    var timestamp_create: Int? = nil
    //    var note: String? = nil
    var rating: Int? = nil
    var in_work: Bool? = nil
    var is_need_rating: Bool = false
    var equipment_markerplace: String? = nil
    var photo: String? = nil
    var imageName: String?
    
    var oldIncident: Incident? = nil
    func setIncident(incident_id: Int){
        self.incident_id = incident_id
    }
    init(incident: Incident? = nil) {
        super.init(nibName: nil, bundle: nil)
        self.oldIncident = incident
        if let incident = incident {
            self.incident_id = incident.incident_id
            self.status_title = incident.status_title
            self.equipment_title = incident.equipment_title
            self.equipment_uin = incident.equipment_uin
            self.equipment_markerplace = incident.equipment_markplace
            self.uin = incident.equipment_uin
            self.timestamp_create = incident.timestamp_create
            self.timestamp_close = incident.timestamp_close
            self.note = incident.problems
            self.rating = incident.rating
            self.is_need_rating = incident.is_need_rating
            self.in_work = incident.in_work
            self.photo = incident.photo
            
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imageName = "PHOTO_INCIDENT_" + self.uin! + "\(self.note)" + ".jpg"
        
        print("url",Database.shared.realm.configuration.fileURL)
        
        self.modalPresentationCapturesStatusBarAppearance = true
        self.view.backgroundColor = Color.Assest.bakground
        self.view.addSubview(self.scrollView)
        
        
        self.valueTexts.titleLabel.text = self.equipment_title
        self.valueTexts.titleLabel.textColor = Color.Assest.text
        self.scrollView.addSubview(self.valueTexts)
        
        
        self.equipmentTitleLabel.text = "УИН:" + self.equipment_uin!
        //        self.equipmentTitleLabel.textColor = Color.Assest.text.withAlphaComponent(0.7)
        self.scrollView.addSubview(self.equipmentTitleLabel)
        if self.equipment_markerplace != nil{
                self.equipmentUinLabel.text = self.equipment_markerplace ?? ""
                self.equipmentUinLabel.textColor = Color.Assest.text.withAlphaComponent(0.7)
                self.scrollView.addSubview(self.equipmentUinLabel)
        }
        
        
        self.scrollView.delegate = self
        self.scrollView.addSubview(self.lineAfterSummView)
        
        
        self.noteLabels.titleLabel.text = Text.problem
        self.noteLabels.titleLabel.textColor = Color.Assest.text
        self.noteLabels.button.setTitleColor(Color.base)
        self.scrollView.addSubview(self.noteLabels)
        
        self.noteTextView.text = self.note
        self.noteTextView.delegate = self
        self.noteTextView.isEditable = false
        self.updateCommentTextView()
        self.textViewDidChange(self.noteTextView)
        self.scrollView.addSubview(self.noteTextView)
  
        
        self.scrollView.addSubview(self.lineAfterButtonsView)
        //
        self.blurView.backgroundColor = Color.Assest.bakground
        self.view.addSubview(self.blurView)
        //
        self.addButton.setTitle(Text.estimate)
        self.addButton.target {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let popStarRatedViewController = storyboard.instantiateViewController(withIdentifier: "PopStarRatedViewController") as! PopStarRatedViewController
            popStarRatedViewController.setIncident(incident_id: self.incident_id!)
            self.present(popStarRatedViewController, animated: false, completion: nil)
        }
        if self.is_need_rating{
            self.view.addSubview(self.addButton)
        }
        
        self.payDateLabels.titleLabel.text = Text.photo
        self.payDateLabels.titleLabel.textColor = Color.Assest.text
        self.payDateLabels.sizeToFit()
        self.payDateLabels.button.target {
            
        }
        
        if FileHelper.getImageCount(imageName: self.imageName!) == true{
            self.scrollView.addSubview(self.payDateLabels)
        }
    
        self.photoView.image = FileHelper.getImage(imageName: self.imageName!)
        self.scrollView.addSubview(self.photoView)
        
        
        self.navBar.titleLabel.text = Text.incident + " #" + incident_id!.description
        self.navBar.rightButton.setTitle(Text.repeal)
        self.navBar.rightButton.setTitleColor(Color.base)
        self.navBar.rightButton.target {
            if self.transitioningDelegate is SPStorkTransitioningDelegate {
                SPStorkController.dismissWithConfirmation(controller: self, completion: nil)
            } else {
                self.dismiss()
            }
        }
  
        self.navBar.separatorView.backgroundColor = Color.Assest.separator
        self.view.addSubview(self.navBar)
        
        self.dismissKeyboardWhenTappedAround()
        
        self.updateLayout(with: self.view.frame.size)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.statusBar = .light
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }
    
    func scrollToNotes(edit: Bool) {
        self.amountAndNoteHorizontalScrollView.setContentOffset(CGPoint.init(x: self.amountAndNoteHorizontalScrollView.frame.width, y: 0), animated: true)
        if edit {
            delay(0.3, closure: {
                self.noteTextView.becomeFirstResponder()
            })
        }
    }
    
    override func updateLayout(with size: CGSize) {
        super.updateLayout(with: size)
        var bottomInsest: CGFloat = 7
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            bottomInsest = window?.safeAreaInsets.bottom ?? 7
        }
        
        self.scrollView.frame = CGRect.init(origin: .zero, size: size)
        self.scrollView.contentInset.top = self.navBar.height
        self.scrollView.scrollIndicatorInsets.top = self.navBar.height
        self.scrollView.scrollIndicatorInsets.bottom = bottomInsest
        self.scrollView.contentInset.bottom = bottomInsest
        
        self.addButton.frame.set(height: Style.buttonHeight)
        self.addButton.frame.set(width: self.modalContentWidth)
        self.addButton.frame.origin.x = Space.modalSide
        self.addButton.frame.bottomY = size.height - bottomInsest - Space.vertical / 2
        
        self.blurView.frame = CGRect.init(
            x: 0, y: 0,
            width: size.width - 20,
            height: (size.height - self.addButton.frame.origin.y - Style.cornerRadius)
        )
        self.blurView.frame.bottomY = size.height
        
        self.valueTexts.layout(origin: CGPoint.init(x: Space.modalSide, y: 0), width: self.modalContentWidth)
        
        
        self.equipmentTitleLabel.sizeToFit()
        self.equipmentTitleLabel.frame.origin.x = Space.modalSide
        self.equipmentTitleLabel.frame.origin.y = self.valueTexts.frame.bottomY + Space.vertical / 2
        
        self.equipmentUinLabel.sizeToFit()
        self.equipmentUinLabel.frame.origin.x = Space.modalSide
        self.equipmentUinLabel.frame.origin.y = self.equipmentTitleLabel.frame.bottomY + Space.vertical / 2
        
        self.lineAfterSummView.frame.set(width: self.modalContentWidth)
        self.lineAfterSummView.frame.origin.x = Space.modalSide
        self.lineAfterSummView.frame.origin.y = self.equipmentUinLabel.frame.bottomY + Space.vertical
        
        
        self.noteLabels.layout(origin: CGPoint.init(x: Space.modalSide, y: self.lineAfterSummView.frame.bottomY + Space.vertical), width: size.width - Space.modalSide * 2)
        
        self.noteTextView.frame = CGRect.init(
            x: self.noteLabels.frame.origin.x,
            y: self.noteLabels.frame.bottomY + Space.vertical / 2,
            width: self.modalContentWidth,
            height: 58
        )
        
        self.noteTextView.frame.set(height: 110.0)
        
        self.lineAfterButtonsView.frame.set(width: self.modalContentWidth)
        self.lineAfterButtonsView.frame.origin.x = Space.modalSide
        self.lineAfterButtonsView.frame.origin.y = self.noteTextView.frame.bottomY + Space.vertical / 1.5
        
        
        //фото
        self.payDateLabels.layout(origin: CGPoint.init(x: Space.modalSide, y: self.lineAfterButtonsView.frame.bottomY + Space.vertical), width: size.width - Space.modalSide * 2)
        
        self.photoView.frame = CGRect.init(
            x: self.payDateLabels.frame.origin.x,
            y: self.payDateLabels.frame.bottomY + Space.vertical / 2,
            width: self.modalContentWidth,
            height: 58
        )
        
        self.photoView.frame.set(height: 300)
        
        let scrollHeight = self.scrollView.frame.bottomY + Space.vertical
        if scrollHeight > self.addButton.frame.origin.y {
            self.scrollView.contentSize = CGSize.init(
                width: size.width,
                height: scrollHeight + self.addButton.frame.height + Space.vertical + bottomInsest
            )
        }
    }
}



extension IncidentViewController: UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        switch textField {
        case self.uinTextField:
            var newText = textField.text?.digits ?? ""
        default:
            break
        }
    }
}

extension IncidentViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        switch scrollView {
        case self.scrollView:
            self.dismissKeyboard()
            SPStorkController.scrollViewDidScroll(scrollView)
        case self.amountAndNoteHorizontalScrollView:
            self.view.endEditing(true)
        default:
            break
        }
    }
}

extension IncidentViewController: UITextViewDelegate {
    
    internal func updateCommentTextView() {
        self.textViewDidChange(self.noteTextView)
        self.textViewDidEndEditing(self.noteTextView)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.note = (textView.text == Text.note_for_incident_placeholder) ? nil : textView.text
        if textView.text.isEmpty {
            self.noteLabels.button.set(enable: false, animatable: false)
        } else {
            if textView.textColor == UIColor.lightGray {
                self.noteLabels.button.set(enable: false, animatable: false)
            } else {
                self.noteLabels.button.set(enable: true, animatable: false)
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
        }
        return true
    }

    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = Color.Assest.text_in_area
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty || (textView.text == Text.note_for_incident_placeholder) {
            textView.text = Text.note_for_incident_placeholder
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case self.uinTextField:
            let char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            
            if (isBackSpace == -92) {
                if let value = Int(textField.text?.digits ?? "") {
                    self.uinTextField.text = String(Int(value / 10)) + "x"
                    if uinTextField.text == "0x" {
                        self.uinTextField.text = nil
                    }
                }
                
            }
            return true
        default:
            return true
        }
    }
}


extension IncidentViewController : UINavigationControllerDelegate {}

protocol IncidentViewDelegate: class {
    
    func updatedIncident( _ incident: Incident?)
}
