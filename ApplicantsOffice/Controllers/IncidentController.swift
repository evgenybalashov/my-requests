//
//  IncidentController.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 31/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import SPFakeBar
import SPAlert
import SPStorkController
import swiftScan
import Photos
import Crashlytics
import RealmSwift
import UserNotifications


class IncidentController: Controller {
    
    let navBar = SPFakeBarView(style: .stork)
    
    let amountAndNoteHorizontalScrollView = SPScrollView()
    
    let valueTexts = SPSectionLabelsView()
    let qrButton = QrButton()
    
    let uinTextField = TextField()
    let noteLabels = SPSectionLabelsView()
    let noteTextView = TextView()
    
    let lineAfterSummView = LineView()
    let createDebtButton = IncidentButton()
    let takeDebtButton = IncidentButton()
    let commentForButtonsLabel = UILabel()
    let lineAfterButtonsView = LineView()
    let userLabel = SPSectionLabelsView()
    let userButton = EquipmentButton()
    
    let payDateAndCreateDateHorizontalScrollView = SPScrollView()
    
    let payDateLabels = SPSectionLabelsView()
    let photoView = UIImageView()
    
    let payDateButton = LightButton()
    let createDateLabels = SPSectionLabelsView()
    let createDateButton = LightButton()
    
    let photoLabels = SPSectionLabelsView()
    var imageName: String?
    
    let addButton = ColorButton()
    let blurView = UIView()
    let scrollView = SPScrollView()
    
    var imageView: UIImageView!
    var imagePicker: UIImagePickerController!
    let pickerController = UIImagePickerController()
    
    weak var delegte: StorageDelegate?
    
    var equipments = [Equipment]()
    var serverIncident = [Incident]()
    
    var equipment: Equipment? = nil {
        didSet {
            if let equipment = self.equipment {
                print("equipment didSet1", equipment.original_id, equipment.equipment_title, equipment.equipment_uin)
                self.uinTextField.text  = equipment.equipment_uin
                self.valueTexts.titleLabel.text = equipment.equipment_title
                self.valueTexts.subtitleLabel.text = equipment.equipment_markplace
                self.equipment_id = equipment.original_id
            }else{
                self.uinTextField.text  = ""
                self.valueTexts.titleLabel.text = ""
                self.valueTexts.subtitleLabel.text = ""
                self.equipment_id = nil
            }
        }
    }
    
    
    var uin: String? = nil {
        didSet {
            if let uin = self.uin {
                print("uin1", uin)
                self.uinTextField.text = uin
            }else{
                self.uinTextField.text = ""
            }
        }
    }

    var equipment_title: String? = nil {
        didSet {
            if let equipment_title = self.equipment_title {
                self.valueTexts.titleLabel.text = equipment_title
            }else{
                self.valueTexts.titleLabel.text = Text.equipment
            }
        }
    }
    
    var equipment_markplace: String? = nil {
        didSet {
            if let equipment_markplace = self.equipment_markplace {
                self.valueTexts.subtitleLabel.text = equipment_markplace
            }else{
                self.valueTexts.subtitleLabel.text = Text.equipment_description
            }
        }
    }
    
    
    var note: String? = nil
    var photo: String? = nil
    var incident_id: Int? = nil
    var equipment_id: Int? = nil
    var is_need_rating: Bool = false
    var oldIncident: Incident? = nil
    
    init(incident: Incident? = nil) {
        super.init(nibName: nil, bundle: nil)
        self.oldIncident = incident
        if let incident = incident {
            self.incident_id = incident.incident_id

            self.equipment_title = incident.equipment_title
            self.equipment_markplace = incident.equipment_markplace
            self.uin = incident.equipment_uin
            self.note = incident.problems
            self.is_need_rating = incident.is_need_rating
            self.photo = incident.photo
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        _ = Location.getInstans().getLatitude()
        _ = Location.getInstans().getLongitude()
        
        UNUserNotificationCenter.current().delegate = self
        
        Api.getEquipment { (error: Error?, equipments: [Equipment]?) in
            if let equipments = equipments {
                self.equipments.append(contentsOf: equipments)
                                print("equipments", self.equipments)
            }
        }
        
        
        if !UIImagePickerController.isSourceTypeAvailable(.camera){
            
            let alertController = UIAlertController.init(title: nil, message: "Device has no camera.", preferredStyle: .alert)
            
            let okAction = UIAlertAction.init(title: "Alright", style: .default, handler: {(alert: UIAlertAction!) in
            })
            
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
        else{
            //other action
            pickerController.sourceType = UIImagePickerController.SourceType.camera
            pickerController.delegate = self as! UIImagePickerControllerDelegate & UINavigationControllerDelegate
        }

        
        self.modalPresentationCapturesStatusBarAppearance = true
        self.view.backgroundColor = Color.Assest.bakground
        self.view.addSubview(self.scrollView)
        
        
        self.valueTexts.titleLabel.text = self.equipment_title != nil ? self.equipment_title : Text.equipment
        //        textView.text == Text.note_for_debt_placeholder) ? nil : textView.text
        self.valueTexts.subtitleLabel.text = self.equipment_markplace != nil ? self.equipment_markplace : Text.equipment_description
        self.valueTexts.titleLabel.textColor = Color.Assest.text
        self.valueTexts.subtitleLabel.textColor = Color.Assest.text.withAlphaComponent(0.7)
        self.scrollView.addSubview(self.valueTexts)
        
        
        self.uinTextField.keyboardType = .default
        self.uinTextField.delegate = self
        self.uinTextField.placeholder = Text.input_example
        
        self.uinTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        self.textFieldDidChange(self.uinTextField)
        self.scrollView.addSubview(self.uinTextField)
        

        self.qrButton.target {
            delay(0.12, closure: {
                self.showQrScanner()
            })
        }
        self.scrollView.addSubview(self.qrButton)
        
        
        
        self.scrollView.delegate = self
        self.scrollView.addSubview(self.lineAfterSummView)
        
        
        self.noteLabels.titleLabel.text = Text.problem
        self.noteLabels.titleLabel.textColor = Color.Assest.text
        self.noteLabels.button.setTitle(Text.clear)
        self.noteLabels.button.setTitleColor(Color.base)
        self.noteLabels.button.target {
            self.noteTextView.text = ""
            self.updateCommentTextView()
        }
        self.scrollView.addSubview(self.noteLabels)
        
        self.noteTextView.returnKeyType = .done
        self.noteTextView.text = self.note
        self.noteTextView.delegate = self
        self.updateCommentTextView()
        self.textViewDidChange(self.noteTextView)
        self.scrollView.addSubview(self.noteTextView)
        
        self.scrollView.addSubview(self.lineAfterButtonsView)
        self.blurView.backgroundColor = Color.Assest.bakground
        self.view.addSubview(self.blurView)

        self.addButton.setTitle(Text.create)
        //MARK: ГОВНОКОД-ПЕРЕДЕЛАТЬ
        self.addButton.target {
            print("incidentController",self.equipments)
            if self.uinTextField.text == "" {
                SPAlert.present(message: Text.no_equipment_specified)
            } else if self.note == nil {
                SPAlert.present(message: Text.no_description)
            }else{
                if self.photoView.image == nil{
                    self.photo = ""
                }else{
                    // если нет директориии, то создаем
                    self.photo = PhotoHelper.convertImageToBase64(image: FileHelper.resize((self.photoView.image)!))
                }
                
                self.equipment = self.equipments.filter {$0.equipment_uin == self.uinTextField.text}.first
                
                self.imageName = "PHOTO_INCIDENT_" + self.uinTextField.text! + "\(self.note)" + ".jpg"
                let parameters: Parameters = [
                    "equipment_uin": self.uin as Any,
                    "equipment_id": self.equipment_id as Any,
                    "problems": self.note as Any,
                    "photo": self.photo as Any,
                    "timestamp_create": Int(NSDate().timeIntervalSince1970),
                    "latitude": Location.getInstans().getLatitude(),
                    "longitude": Location.getInstans().getLongitude()
                ]
                self.dismiss()
                SPAlert.present(title: Text.added, message: nil, preset: .done)
                if !Connectivity.isConnectedToInternet{
                    Storage.save(incident: self.incidentNotInet()!)
                }else{
                    
                    let queue = DispatchQueue.global(qos: .utility)
                    queue.async{
                        DispatchQueue.main.async {
                            Api.getIncidentCreate(parameters: parameters) { (error: Error?, answer: Int?) in
                                if let answer = answer{
                                    self.incident_id = answer
                                }
                                if (self.incident_id != -1){
                                    Api.getIncident(id: self.incident_id!, hash: self.incident_id! ) { (error: Error?, incident: Incident?) in
                                        if let incident = incident{
                                            self.serverIncident.append(incident)
                                        }
                                        Storage.save(incident: incident!)
                                        self.delegte?.storageUpdated()
                                        
                                    }
                                    
                                    FileHelper.createDirectory()
                                    if self.photoView.image != nil{
                                        if FileHelper.getImageCount(imageName: self.imageName!) == true{
                                            FileHelper.deleteSaveImage(imageName: self.imageName!)
                                        }
                                        FileHelper.saveImageToDocumentDirectory(imageName: self.imageName!, image: self.photoView.image!)
                                    }
                                    
                                }else{
                                    Api.getNotifiList { (error: Error?, notifi: [Notifi]?) in
                                        if let notifi = notifi {
                                            for noti in notifi{
                                                Notification.createIncident(title: noti.title, body: noti.body)
                                                let parameters: Parameters = [
                                                    "id": noti.notifi_id
                                                ]
                                                Api.getComplete(parameters: parameters) { (error:Error?, success:String?) in
                                                }
                                            }
                                            self.delegte?.storageUpdated()
                                        }
                                    }
                                }
                            }
                            print("Show image data")
                        }
                        print("Did download  image data")
                    }
                }
            }
        }
        self.view.addSubview(self.addButton)
        
        self.payDateLabels.titleLabel.text = Text.photo
        self.payDateLabels.titleLabel.textColor = Color.Assest.text
        self.payDateLabels.button.setTitle(Text.add)
        self.payDateLabels.button.setTitleColor(Color.base)
        self.payDateLabels.sizeToFit()
        self.payDateLabels.button.target {
            self.chooseOtherUserSource()
        }
        self.scrollView.addSubview(self.payDateLabels)

        self.scrollView.addSubview(self.photoView)
        
        self.navBar.titleLabel.text = Text.new_incidents
        self.navBar.rightButton.setTitle(Text.cancel)
        self.navBar.rightButton.setTitleColor(Color.base)
        self.navBar.rightButton.target {
            if self.transitioningDelegate is SPStorkTransitioningDelegate {
                SPStorkController.dismissWithConfirmation(controller: self, completion: nil)
            } else {
                self.dismiss()
            }
        }
        self.navBar.separatorView.backgroundColor = Color.Assest.separator
        self.view.addSubview(self.navBar)
        
        self.dismissKeyboardWhenTappedAround()
        
        self.updateLayout(with: self.view.frame.size)
    }
    
    func incidentNotInet() -> Incident? {
        
        let incident = Incident.init(
            incident_id: 0,
            crc32_hash: 0,
            status_title: "",
            timestamp_create: Int(NSDate().timeIntervalSince1970),
            equipment_title: "",
            equipment_uin: self.uinTextField.text,
            equipment_markplace: "",
            in_work: true,
            timestamp_close: 0,
            problems: note,
            additional_info: "",
            is_need_rating: false,
            rating: 0,
            rating_comment: "",
            photo: "")
        
        return incident
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.statusBar = .light
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func scrollToNotes(edit: Bool) {
        self.amountAndNoteHorizontalScrollView.setContentOffset(CGPoint.init(x: self.amountAndNoteHorizontalScrollView.frame.width, y: 0), animated: true)
        if edit {
            delay(0.3, closure: {
                self.noteTextView.becomeFirstResponder()
            })
        }
    }
    
    override func updateLayout(with size: CGSize) {
        super.updateLayout(with: size)
        var bottomInsest: CGFloat = 7
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            bottomInsest = window?.safeAreaInsets.bottom ?? 7
        }
        
        self.scrollView.frame = CGRect.init(origin: .zero, size: size)
        self.scrollView.contentInset.top = self.navBar.height
        self.scrollView.scrollIndicatorInsets.top = self.navBar.height
        self.scrollView.scrollIndicatorInsets.bottom = bottomInsest
        self.scrollView.contentInset.bottom = bottomInsest
        
        self.addButton.frame.set(height: Style.buttonHeight)
        self.addButton.frame.set(width: self.modalContentWidth)
        self.addButton.frame.origin.x = Space.modalSide
        self.addButton.frame.bottomY = size.height - bottomInsest - Space.vertical / 2
        
        self.blurView.frame = CGRect.init(
            x: 0, y: 0,
            width: size.width - 20,
            height: (size.height - self.addButton.frame.origin.y - Style.cornerRadius)
        )
        self.blurView.frame.bottomY = size.height
        
        self.valueTexts.layout(origin: CGPoint.init(x: Space.modalSide, y: 0), width: self.modalContentWidth)
        
        let currencySpace = Space.side / 2
        self.qrButton.frame = CGRect.init(x: 0, y: 0, width: Style.buttonHeight, height: Style.buttonHeight)
        self.qrButton.frame.bottomX = size.width - Space.modalSide
        self.qrButton.frame.origin.y = self.valueTexts.frame.bottomY + Space.vertical / 2
        
        self.uinTextField.sizeToFit()
        self.uinTextField.frame.set(width: self.qrButton.frame.origin.x - currencySpace - Space.modalSide)
        self.uinTextField.frame.origin.x = Space.modalSide
        self.uinTextField.frame.origin.y = self.valueTexts.frame.bottomY + Space.vertical / 2
        
        self.lineAfterSummView.frame.set(width: self.modalContentWidth)
        self.lineAfterSummView.frame.origin.x = Space.modalSide
        self.lineAfterSummView.frame.origin.y = self.uinTextField.frame.bottomY + Space.vertical
        
        self.noteLabels.layout(origin: CGPoint.init(x: Space.modalSide, y: self.lineAfterSummView.frame.bottomY + Space.vertical), width: size.width - Space.modalSide * 2)
        
        self.noteTextView.frame = CGRect.init(
            x: self.noteLabels.frame.origin.x,
            y: self.noteLabels.frame.bottomY + Space.vertical / 2,
            width: self.modalContentWidth,
            height: 58
        )
        
        self.noteTextView.frame.set(height: self.uinTextField.frame.bottomY - self.valueTexts.frame.origin.y)
        
        self.lineAfterButtonsView.frame.set(width: self.modalContentWidth)
        self.lineAfterButtonsView.frame.origin.x = Space.modalSide
        self.lineAfterButtonsView.frame.origin.y = self.noteTextView.frame.bottomY + Space.vertical / 1.5
        
        
        //фото
        self.payDateLabels.layout(origin: CGPoint.init(x: Space.modalSide, y: self.lineAfterButtonsView.frame.bottomY + Space.vertical), width: size.width - Space.modalSide * 2)
        
        self.photoView.frame = CGRect.init(
            x: self.payDateLabels.frame.origin.x,
            y: self.payDateLabels.frame.bottomY + Space.vertical / 2,
            width: self.modalContentWidth,
            height: 58
        )
        
        self.photoView.frame.set(height: 300)

        
        let scrollHeight = self.scrollView.frame.bottomY + Space.vertical
        if scrollHeight > self.addButton.frame.origin.y {
            self.scrollView.contentSize = CGSize.init(
                width: size.width,
                height: scrollHeight + self.addButton.frame.height + Space.vertical + bottomInsest
            )
        }
    }
    
    func showQrScanner() {
        let controller = QQScanViewController()
        var style = LBXScanViewStyle()
        style.animationImage = UIImage(named: "CodeScan.bundle/qrcode_scan_light_green")
        controller.scanStyle = style
        
        controller.delegate = self
        
        if Flags.useStorkPresentation {
            let transitionDelegate = SPStorkTransitioningDelegate()
            transitionDelegate.translateForDismiss = 80
            transitionDelegate.showIndicator = false
            controller.transitioningDelegate = transitionDelegate
            controller.modalPresentationStyle = .custom
            self.present(controller)
        } else {
            controller.modalPresentationStyle = .formSheet
            controller.preferredContentSize = CGSize.init(width: 420, height: 570)
            self.present(controller, animated: true, completion: nil)
        }
        
    }
    func scanFinished(scanResult: LBXScanResult, error: String?) {
        NSLog("scanResult:\(scanResult)")
    }

    func uploadImageCamera(){
        pickerController.sourceType = UIImagePickerController.SourceType.camera
        present(pickerController, animated: true, completion: nil)
    }
    
    func uploadImageLibrary(){
        pickerController.sourceType = .photoLibrary
        pickerController.allowsEditing = false
        present(pickerController, animated: true, completion: nil)
    }
    
    
    @objc func longPressUserButton(gester: UILongPressGestureRecognizer) {
        if gester.state == .began {
            SPVibration.impact(.light)
            self.chooseOtherUserSource()
        }
    }
    
    private func chooseOtherUserSource() {
        let controller = UIAlertController.init(title: Text.photo_title, message: Text.photo_message, preferredStyle: .actionSheet)
        controller.addAction(title: Text.camera, complection: {
            self.uploadImageCamera()
        })
        controller.addAction(title: Text.gallery, complection: {
            self.uploadImageLibrary()

            
        })
        
        controller.addCancelAction(title: Text.cancel)
        self.present(controller)
        
    }
}


extension IncidentController: ChooseEquipmentControllerDelegate {
    
    func choose(equipment: Equipment) {
        self.equipment = equipment
        SPStorkController.updatePresentingController(parent: self)
    }
    
    func noChoose(uin: UITextField){
        self.uin = uin.text
    }
}
var gameTimer: Timer?

extension IncidentController: UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        switch textField {
        case self.uinTextField:
            if textField.text == ""{
                self.valueTexts.titleLabel.text = Text.equipment
                self.valueTexts.subtitleLabel.text = Text.equipment_description
            }
            
            if gameTimer != nil{
                gameTimer?.invalidate()
            }
            gameTimer = Timer.scheduledTimer(withTimeInterval: 2.0, repeats: false, block: {timer in
                var filterEquipment = [Equipment]()
                filterEquipment = self.equipments.filter { $0.equipment_uin.contains((textField.text?.description)!) }
                print("filterEquipment", filterEquipment.count)
                
                if filterEquipment.count > 2 {
                        let controller = ChooseEquipmentController()
                        controller.setEquipments(equipments: filterEquipment)
                        controller.choseEquipmentDelegate = self
                        controller.modalPresentationCapturesStatusBarAppearance = true
                        controller.modalPresentationStyle = .custom
                        let transitionDelegate = SPStorkTransitioningDelegate()
                        controller.transitioningDelegate = transitionDelegate
                        transitionDelegate.customHeight = controller.height + self.safeArea.bottom
                        transitionDelegate.translateForDismiss = 80
                        transitionDelegate.showIndicator = false
                        transitionDelegate.indicatorColor = Color.Assest.indicator
                        self.present(controller)
                }
                if filterEquipment.count == 1{
                    self.choose(equipment: filterEquipment.first!)
                }
                if filterEquipment.count == 0{
                    self.noChoose(uin: textField)
                    
                }
            })
            
            
        default:
            break
        }
    }
}

extension StringProtocol {
    func index<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> Index? {
        range(of: string, options: options)?.lowerBound
    }
    func endIndex<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> Index? {
        range(of: string, options: options)?.upperBound
    }
    func indices<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> [Index] {
        var indices: [Index] = []
        var startIndex = self.startIndex
        while startIndex < endIndex,
            let range = self[startIndex...]
                .range(of: string, options: options) {
                    indices.append(range.lowerBound)
                    startIndex = range.lowerBound < range.upperBound ? range.upperBound :
                        index(range.lowerBound, offsetBy: 1, limitedBy: endIndex) ?? endIndex
        }
        return indices
    }
    func ranges<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> [Range<Index>] {
        var result: [Range<Index>] = []
        var startIndex = self.startIndex
        while startIndex < endIndex,
            let range = self[startIndex...]
                .range(of: string, options: options) {
                    result.append(range)
                    startIndex = range.lowerBound < range.upperBound ? range.upperBound :
                        index(range.lowerBound, offsetBy: 1, limitedBy: endIndex) ?? endIndex
        }
        return result
    }
}

extension IncidentController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        switch scrollView {
        case self.scrollView:
            self.dismissKeyboard()
            SPStorkController.scrollViewDidScroll(scrollView)
        case self.amountAndNoteHorizontalScrollView:
            self.view.endEditing(true)
        default:
            break
        }
    }
}

extension IncidentController: UITextViewDelegate {
    
    internal func updateCommentTextView() {
        self.textViewDidChange(self.noteTextView)
        self.textViewDidEndEditing(self.noteTextView)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.note = (textView.text == Text.note_for_incident_placeholder) ? nil : textView.text
        if textView.text.isEmpty {
            self.noteLabels.button.set(enable: false, animatable: false)
        } else {
            if textView.textColor == UIColor.lightGray {
                self.noteLabels.button.set(enable: false, animatable: false)
            } else {
                self.noteLabels.button.set(enable: true, animatable: false)
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
        }
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = Color.Assest.text_in_area
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty || (textView.text == Text.note_for_incident_placeholder) {
            textView.text = Text.note_for_incident_placeholder
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
}

extension IncidentController: QRRectDelegate {
    func drawwed() {}
    func uin(uin: String) {
        self.uin = uin
        let equipment = self.equipments.filter {$0.equipment_uin == uin}.first
        self.equipment_title = equipment?.equipment_title
        self.equipment_markplace = equipment?.equipment_markplace
        SPStorkController.updatePresentingController(parent: self)
    }
}

extension IncidentController : UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        photoView.image = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

extension IncidentController : UINavigationControllerDelegate {}

protocol IncidentDelegate: class {
    
    func updatedIncident( _ incident: Incident?)
}

extension IncidentController: SPStorkControllerConfirmDelegate {
    
    var needConfirm: Bool {
        
        guard Flags.isConfirm else { return false }
        
        if self.oldIncident == nil {
            let data: [Any?] = [self.uin, self.note]
            for item in data {
                if item != nil {
                    return true
                }
            }
        } else {
            if self.uin != self.oldIncident?.equipment_uin { return true }
            if self.note != self.oldIncident?.problems { return true }
        }
        
        return false
    }
    
    func confirm(_ completion: @escaping (Bool) -> ()) {
        let alertController = UIAlertController(title: Text.unsaved_changes, message: Text.unsaved_changes_decsription, preferredStyle: .actionSheet)
        alertController.addDestructiveAction(title: Text.still_close, complection: {
            completion(true)
        })
        alertController.addCancelAction(title: Text.cancel, complection: {
            completion(false)
        })
        self.present(alertController)
        SPVibration.impact(.warning)
    }
}

extension IncidentController : UNUserNotificationCenterDelegate{
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert,.sound,.badge])
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler:
        @escaping () -> Void) {
        let url = ""
        if let requestUrl = URL(string: url), UIApplication.shared.canOpenURL(requestUrl) {
            guard let appFullPathUrl = URL(string: url) else { return }
            UIApplication.shared.open(appFullPathUrl, options: [:], completionHandler: nil)
        }
        // Always call the completion handler when done.
        completionHandler()
    }
}

