//
//  LicenseAgreement.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 25/11/2019.
//  Copyright © 2019 example. All rights reserved.
//

import Foundation
import UIKit
import FRHyperLabel


class LicenseAgreementController: UIViewController
{
    @IBOutlet weak var licenseLabel: FRHyperLabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        licenseLabel.numberOfLines = 0;
        
        let string = "Я даю Согласие на обработку персональных данных и принимаю Правила использования приложения"
        
        licenseLabel.textColor = UIColor.white
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white,
                          NSAttributedString.Key.underlineColor: UIColor.orange,
                          NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.headline)]
        
        
        licenseLabel.attributedText = NSAttributedString(string: string, attributes: attributes)
        licenseLabel.linkAttributeDefault = [NSAttributedString.Key.foregroundColor: Color.orangeNew]
        
        
        let handler = {
            (hyperLabel: FRHyperLabel?, substring: String?) -> Void in
          if let url = URL(string: "https://#/License.pdf") {
              UIApplication.shared.open(url)
          }
        }
        
        licenseLabel.setLinksForSubstrings(["Согласие на обработку персональных данных", "Правила использования приложения"], withLinkHandler: handler)
    }
    
     @IBAction func clickLicense(_ sender: Any){
        Preferences.getInstans().setLicense(license: "ok")
        print("Preferences.getInstans().setLicense(license:)", Preferences.getInstans().getLicense())
    }
}
