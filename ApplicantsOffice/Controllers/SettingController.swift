//
//  SettingController.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 13/11/2019.
//  Copyright © 2019 example. All rights reserved.
//

import Eureka
import UserNotifications

class SettingController: FormViewController
{
    var navigationOptionsBackup : RowNavigationOptions?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UNUserNotificationCenter.current().delegate = self
        
        self.view.backgroundColor = Color.Assest.bakground
        self.setNavigationTitle(Text.menu, style: .small)
        
        navigationOptions = RowNavigationOptions.Enabled.union(.SkipCanNotBecomeFirstResponderRow)
        navigationOptionsBackup = navigationOptions
        
        LabelRow.defaultCellUpdate = { cell, row in
            cell.contentView.backgroundColor = .red
            cell.textLabel?.textColor = .white
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 13)
            cell.textLabel?.textAlignment = .right
            
        }
        
        TextRow.defaultCellUpdate = { cell, row in
            if !row.isValid {
                cell.titleLabel?.textColor = .red
            }
        }
        
        
        form = Section(header: "Настройки", footer: "Уведомления помогут быстро получать информацию")
            
            <<< SwitchRow("set_none") { [weak self] in
                $0.title = "Уведомления"
                $0.value = self?.navigationOptions != .Disabled
            }.onChange { [weak self] in
                if $0.value ?? false {

                }
                else {
                }
            }
            
            +++ Section()
            <<< ButtonRow() {
                $0.title = "Обратная связь"
            }
            .onCellSelection { cell, row in
                Feedback.open(on: self)
        }
        
        
    }
    
    
}

extension FormViewController{
    func showFeedBackDialog(title:String? = nil,
                            subtitle:String? = nil,
                            actionTitle:String? = "Add",
                            cancelTitle:String? = "Cancel",
                            inputPlaceholder:String? = nil,
                            inputKeyboardType:UIKeyboardType = UIKeyboardType.default,
                            cancelHandler: ((UIAlertAction) -> Swift.Void)? = nil,
                            actionHandler: ((_ text: String?) -> Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: subtitle, preferredStyle: .alert)
        alert.addTextField { (textField:UITextField) in
            textField.placeholder = inputPlaceholder
            textField.keyboardType = inputKeyboardType
        }
        alert.addAction(UIAlertAction(title: actionTitle, style: .destructive, handler: { (action:UIAlertAction) in
            guard let textField =  alert.textFields?.first else {
                actionHandler?(nil)
                return
            }
            actionHandler?(textField.text)
        }))
        alert.addAction(UIAlertAction(title: cancelTitle, style: .cancel, handler: cancelHandler))
        
        self.present(alert, animated: true, completion: nil)
    }
}

extension SettingController : UNUserNotificationCenterDelegate{
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert,.sound,.badge])
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler:
        @escaping () -> Void) {
        let url = ""
        if let requestUrl = URL(string: url), UIApplication.shared.canOpenURL(requestUrl) {
            guard let appFullPathUrl = URL(string: url) else { return }
            UIApplication.shared.open(appFullPathUrl, options: [:], completionHandler: nil)
        }
        // Always call the completion handler when done.
        completionHandler()
    }
}
