//
//  ChooseEquipmentController.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 20/11/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit
import SPFakeBar
import SPStorkController


class ChooseEquipmentController: EquipmentListController{
    
    let navBar = SPFakeBarView(style: .stork)
    override func setEquipments(equipments: [Equipment]){
        self.equipments = equipments
    }
    weak var choseEquipmentDelegate: ChooseEquipmentControllerDelegate?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.navBar.titleLabel.text = Text.equipment
        self.navBar.leftButton.setTitle(Text.cancel)
        self.navBar.leftButton.setTitleColor(Color.base)
        self.navBar.leftButton.target { self.dismiss() }
        self.navBar.closeButtonPossition = .left
        self.view.addSubview(self.navBar)
        
        self.collectionView.contentInset.top = self.navBar.height + Space.vertical / 2
        self.collectionView.scrollIndicatorInsets.top = self.navBar.height
        self.collectionView.scrollIndicatorInsets.bottom = self.safeArea.bottom
        self.collectionView.contentInset.bottom = Space.vertical
        
        self.updateLayout(with: self.view.frame.size)
    }
    
    override func updateLayout(with size: CGSize) {
        super.updateLayout(with: size)
        self.collectionView.frame = CGRect.init(origin: .zero, size: size)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.collectionView {
            self.choseEquipmentDelegate?.choose(equipment: self.equipments[indexPath.row])
            self.dismiss()
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        SPStorkController.scrollViewDidScroll(scrollView)
    }
}

protocol ChooseEquipmentControllerDelegate: class {
    
    func choose(equipment: Equipment)
}
