//
//  IncidentListController.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 31/10/2019.
//  Copyright © 2019 example. All rights reserved.
//
//
import UIKit
import SPStorkController
import SPAlert
import PromiseKit
import RealmSwift
import Alamofire
import UserNotifications

class IncidentsListController: Controller {
    
    let collectionView = IncidentCollectionView()
    var is_need = true
    var incidents: [Incident] = Storage.incidents()
    var newIncidents: [Hash] = []
    var hashs = [Hash]()
    
    weak var delegate: StorageDelegate?
    
    init(incidents: [Incident]) {
        super.init(nibName: nil, bundle: nil)
    }
    //обновление заданий
    lazy var refrecher: UIRefreshControl = {
        let refrechControl = UIRefreshControl()
        refrechControl.tintColor = .black
        refrechControl.attributedTitle = NSAttributedString(string: "Обновление")
        refrechControl.addTarget(self, action: #selector(self.requestData), for: .valueChanged)
        return refrechControl
    }()
    
    @objc func requestData(){
        fetch()
        self.refrecher.endRefreshing()
    }
    //MARK: Переделать
    func fetch(){
        if Connectivity.isConnectedToInternet{
            Api.getIncidentListHash {(error: Error?, hashs: [Hash]?) in
                if let hashs = hashs {
                    self.hashs.append(contentsOf: hashs)
                    
                    if self.incidents.count == 0{
                        for hash in self.hashs{
                            Api.getIncident(id: hash.incident_id, hash: hash.crc32_hash) { (error: Error?, incident: Incident?) in
                                if let incident = incident{
//                                    Storage.delete(incident_id: hash.incident_id)
                                    Storage.save(incident: incident)
                                    self.storageUpdated()
                                }
                            }
                        }
                    }else{
                        for hash in self.hashs{
                            print("hashs", hash.incident_id, hash.crc32_hash)
                            for incident in self.incidents{
                                print("incident", incident.incident_id, incident.crc32_hash)
                                if hash.incident_id == incident.incident_id && hash.crc32_hash != incident.crc32_hash{
                                    Api.getIncident(id: hash.incident_id, hash: hash.crc32_hash) { (error: Error?, incident: Incident?) in
                                        if let incident = incident{
//                                            Storage.delete(incident_id: incident.incident_id)
                                            Storage.save(incident: incident)
                                            self.storageUpdated()
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }else{
            SPAlert.present(message: "Не можем скачать обновления. Нет подключения к интернету")
            self.storageUpdated()
        }
    }
    //MARK: ПЕРЕДЕЛАТЬ
    func incidentSending(){
        var incident_id: Int? = nil
        let equipment_id: Int? = nil
        //        var image:String? = nil
        //        let imageName: String? = "PHOTO_INCIDENT_" + incident.equipment_uin! + "\(NSDate().timeIntervalSince1970))" + ".jpg"
        //        if imageName != nil {
        //            image = ""
        //                PhotoHelper.convertImageToBase64(image: FileHelper.getImage(imageName: imageName!))
        //        }else{
        //            image = ""
        //        }
        let incidents: [Incident] = Storage.incidentNoInternet()
        print("Storage.incidentNoInternet()", Storage.incidentNoInternet())
        var incidentNil: Incident?
        for incident in incidents {
            if incident.incident_id == 0 {
                
                let parameters: Parameters = [
                    "equipment_uin": incident.equipment_uin as Any,
                    "equipment_id": equipment_id as Any,
                    "problems": incident.problems as Any,
                    "photo": "",
                    "timestamp_create": incident.timestamp_create,
                    "latitude": Location.getInstans().getLatitude(),
                    "longitude": Location.getInstans().getLongitude()
                ]
                print("param", parameters)
                
                Api.getIncidentCreate(parameters: parameters) { (error: Error?, answer: Int?) in
                    if let answer = answer{
                        incident_id = answer
                    }
                    Storage.delete(incident_id: 0)
                    //                self.storageUpdated()
                    if (incident_id != -1){
                        Api.getIncident(id: incident_id!, hash: incident_id!) { (error: Error?, incident: Incident?) in
                            if let incident = incident
                            {
                                incidentNil = incident
                            }
                            Storage.save(incident: incidentNil!)
                            self.storageUpdated()
                        }
                    }else{
                        Api.getNotifiList { (error: Error?, notifi: [Notifi]?) in
                            if let notifi = notifi {
                                for noti in notifi{
                                    Notification.createIncident(title: noti.title, body: noti.body)
                                }
                                self.storageUpdated()
                            }
                        }
                    }
                }
            }
        }
        self.fetch()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("viewWillAppear")
        self.storageUpdated()
        self.incidentSending()
        //        self.fetch()
        
        
        
        delay(5.0, closure: {
            let needRatingIncidents = self.incidents.filter{$0.is_need_rating == true}
            
            for needRatingIncident in needRatingIncidents{
                if self.is_need {
                    self.alert(needRatingIncident: needRatingIncident)
                    continue
                }else{
                    break
                }
                
            }
        })
        
        
    }
    func allPerfectly(id: Int, completion: @escaping (_ error: Error?, _ success: Bool?  )->Void){
        let queue = DispatchQueue.global(qos: .utility)
        queue.async{
            DispatchQueue.main.async {
                let parameters: Parameters = [
                    "incident_id": id,
                    "rating":  5,
                    "comment": ""
                ]
                
                let dict: [String: Any] = ["rating": 5,
                                           "is_need_rating": false,
                                           "rating_comment": ""]
                Storage.update(incident_id: id, with: dict)
                
                Api.setIncidentRating(parameters: parameters) { (error: Error?, success: String?) in
                }
            }
        }
        
        var success = Bool()
        success = true
        print("success", success)
        completion(nil, success)
    }
    
    func redirectToIncidentID(needRatingIncident: Incident){
        self.is_need = false
        self.showView(incident: needRatingIncident)
    }
    
    
    func alert(needRatingIncident: Incident){
        let alertController = UIAlertController(title: "Пожалуйста, оцените наше обслуживание ", message: "Оцените проведенные работы по инциденту #\(needRatingIncident.incident_id)", preferredStyle: .alert)
        
        let fine = UIAlertAction.init(title: "Все отлично", style: .default , handler: { action in
            self.allPerfectly(id: needRatingIncident.incident_id) { (error: Error?, success: Bool?) in
                if let success = success{
                    self.is_need = success
                    print("is_need", self.is_need)
                }
            }
        })
        let ok = UIAlertAction.init(title: "Позже", style: .cancel, handler: { action in
            print(action)
            self.is_need = false
            
        })
        let destructive = UIAlertAction.init(title: "Подробнее", style: .default, handler: { action in
            self.redirectToIncidentID(needRatingIncident: needRatingIncident)
            print(action)
        })
        alertController.addAction(fine)
        alertController.addAction(ok)
        alertController.addAction(destructive)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        print("viewDidDisappear")
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("viewDidAppear")
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
         UNUserNotificationCenter.current().delegate = self
        _ = Location.getInstans().getLatitude()
        _ = Location.getInstans().getLongitude()
        print("inxident", incidents.count)
        self.fetch()
        self.storageUpdated()
        self.view.backgroundColor = Color.Assest.bakground
        self.setNavigationTitle(Text.incident, style: .stork)
        
        //        navigationItem.title = Text.incident
        //        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.backgroundColor = UIColor(red: 0/255, green: 166/255, blue: 90/255, alpha: 1.0)
            appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            
            UINavigationBar.appearance().tintColor = .white
            UINavigationBar.appearance().standardAppearance = appearance
            UINavigationBar.appearance().compactAppearance = appearance
            UINavigationBar.appearance().scrollEdgeAppearance = appearance
        } else {
            UINavigationBar.appearance().tintColor = .white
            UINavigationBar.appearance().barTintColor = UIColor(red: 0/255, green: 166/255, blue: 90/255, alpha: 1.0)
            UINavigationBar.appearance().isTranslucent = false
        }
        
        if #available(iOS 11.0, *) {
            self.collectionView.contentInsetAdjustmentBehavior = .always
        }
        self.collectionView.contentInset.top = Space.side / 2
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.refreshControl = refrecher
        self.collectionView.layout.scrollDirection = .vertical
        self.collectionView.contentInset.bottom = Space.side
        self.collectionView.layout.maxItemSpace = Space.side / 2
        self.collectionView.layout.minItemSpace = Space.side / 2
        self.collectionView.showsVerticalScrollIndicator = true
        self.collectionView.layout.maxWidth = self.view.frame.width - (Space.side)
        self.collectionView.layout.cellSideRatio = 316 / 165
        self.view.addSubview(self.collectionView)
        
        self.emptyTitlesView.titleLabel.text = Text.empty
        self.emptyTitlesView.subtitleLabel.text = Text.no_incident
        
        self.collectionView.emptyPlaceholderView.removeFromSuperview()
        
        self.updateEmptyLabel()
        
        self.updateLayout(with: self.view.frame.size)
    }
    
    @objc func addTapped(){
        self.show(incident: nil)
    }
    
    override func updateLayout(with size: CGSize) {
        super.updateLayout(with: size)
        self.collectionView.frame = CGRect.init(origin: .zero, size: size)
        self.collectionView.layout.maxWidth = self.collectionView.frame.width - (Space.side)
    }
    
    private func updateData() {
        self.incidents = Storage.incidents()
        self.collectionView.reloadData()
        
        self.updateEmptyLabel()
    }
    
    func updateEmptyLabel() {
        if self.incidents.isEmpty {
            self.emptyTitlesView.isHidden = false
        } else {
            self.emptyTitlesView.isHidden = true
        }
    }
    
    @objc func changeSegment() {
        SPVibration.impact(.light)
        self.collectionView.reloadData()
        self.updateEmptyLabel()
    }
    
    override func storageUpdated() {
        self.updateData()
        SPStorkController.updatePresentingController(parent: self)
        self.delegate?.storageUpdated()
    }
}

extension IncidentsListController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.incidents.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueIncidentCell(indexPath: indexPath)
        cell.set(incident: self.incidents[indexPath.row], controller: self)
        
        if indexPath.row != (self.collectionView.numberOfItems(inSection: 0) - 1) {
            cell.customShadowOpacity = 0.1
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.showActions(for: self.incidents[indexPath.row])
    }
}

extension IncidentsListController : UNUserNotificationCenterDelegate{

    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {

        completionHandler([.alert,.sound,.badge])
    }


    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler:
        @escaping () -> Void) {
        let url = "itms-services://?action=download-manifest&url=https://#/ios-app/manifest.plist"

        if let requestUrl = URL(string: url), UIApplication.shared.canOpenURL(requestUrl) {
            guard let appFullPathUrl = URL(string: url) else { return }
            UIApplication.shared.open(appFullPathUrl, options: [:], completionHandler: nil)
        }
        // Always call the completion handler when done.
        completionHandler()
    }
}


protocol IncidentListDelegate: class {
    
    func updatedIncident( _ incident: Incident?)
}
