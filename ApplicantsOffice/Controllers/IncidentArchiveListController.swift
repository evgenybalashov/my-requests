//
//  IncidentArchiveListController.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 07/11/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit
import SPStorkController

class IncidentArchiveListController: Controller {
     
    let collectionView = IncidentCollectionView()

    var createIncidents: [Incident] = []

    var incidents: [Incident] = Storage.archiveIncidents()

        weak var delegate: StorageDelegate?

        init(incidents: [Incident]) {
            super.init(nibName: nil, bundle: nil)
            self.createIncidents = incidents
        }
        
        required init?(coder aDecoder: NSCoder) {
              super.init(coder: aDecoder)
           }
    
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
         print("viewWillAppear")
         self.storageUpdated()
     }
        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.storageUpdated()
            
            self.view.backgroundColor = Color.Assest.bakground
            self.setNavigationTitle(Text.archive, style: .small)

            if #available(iOS 11.0, *) {
                self.collectionView.contentInsetAdjustmentBehavior = .always
            }
            self.collectionView.contentInset.top = Space.side / 2

            self.collectionView.dataSource = self
            self.collectionView.delegate = self
            self.collectionView.layout.scrollDirection = .vertical
            self.collectionView.contentInset.bottom = Space.side
            self.collectionView.layout.maxItemSpace = Space.side / 2
            self.collectionView.layout.minItemSpace = Space.side / 2
            self.collectionView.showsVerticalScrollIndicator = true
            self.collectionView.layout.maxWidth = self.view.frame.width - (Space.side)
            self.collectionView.layout.cellSideRatio = 316 / 165
            self.view.addSubview(self.collectionView)

            self.emptyTitlesView.titleLabel.text = Text.empty
            self.emptyTitlesView.subtitleLabel.text = Text.no_incident

            self.collectionView.emptyPlaceholderView.removeFromSuperview()
            
            self.updateEmptyLabel()

            self.updateLayout(with: self.view.frame.size)

        }
        
        
        @objc func addTapped(){
            self.show(incident: nil)
           }

        override func updateLayout(with size: CGSize) {
            super.updateLayout(with: size)
            self.collectionView.frame = CGRect.init(origin: .zero, size: size)
            self.collectionView.layout.maxWidth = self.collectionView.frame.width - (Space.side)
        }

        private func updateData() {
            self.incidents = Storage.archiveIncidents()
            print("updateData incidents", incidents.first?.incident_id, incidents.first?.rating)
            self.createIncidents = incidents

            self.collectionView.reloadData()

            self.updateEmptyLabel()
        }

        func updateEmptyLabel() {
            if self.incidents.isEmpty {
                self.emptyTitlesView.isHidden = false
            } else {
                self.emptyTitlesView.isHidden = true
            }
        }

        @objc func changeSegment() {
            SPVibration.impact(.light)
            self.collectionView.reloadData()
            self.updateEmptyLabel()
        }

        override func storageUpdated() {
            self.updateData()
            SPStorkController.updatePresentingController(parent: self)
            self.delegate?.storageUpdated()
        }
    }

    extension IncidentArchiveListController: UICollectionViewDataSource, UICollectionViewDelegate {

        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return self.incidents.count
        }

        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = self.collectionView.dequeueIncidentCell(indexPath: indexPath)
            cell.set(incident: self.incidents[indexPath.row], controller: self)
        
            if indexPath.row != (self.collectionView.numberOfItems(inSection: 0) - 1) {
                cell.customShadowOpacity = 0.1
            }

            return cell
        }

        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            self.showActions(for: self.incidents[indexPath.row])
        }
    }

    protocol IncidentArchiveListDelegate: class {

        func updatedIncident( _ incident: Incident?)
    }

