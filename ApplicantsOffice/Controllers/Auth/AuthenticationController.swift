//
//  AuthenticationController.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 08/11/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit
import SPAlert
import Alamofire

class AuthenticationController: Controller {
    //MARK: Переделать
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    var phoneNumber: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneNumberTextField.becomeFirstResponder()
        phoneNumberTextField.keyboardType = .phonePad
    }
    
    
    @IBAction func auth(_ sender: Any){
        
        phoneNumber = phoneNumberTextField.text
        
        if (phoneNumber!.isEmpty || phoneNumber?.count != 12){
            SPAlert.present(message: "Введите номер телефона")
            return
        }
        
        let value: NSMutableString = NSMutableString(string: phoneNumber!)
        let pattern = "(\\+7)(\\d{3})(\\d{3})(\\d{2})(\\d{2})"
        let regex = try? NSRegularExpression(pattern: pattern)
        regex?.replaceMatches(in: value, options: .reportProgress, range: NSRange(location: 0,length: value.length), withTemplate: "$1 $2 $3-$4-$5")
         
        let parameters: Parameters = [
             "cell_phone": value
         ]
        
        self.requestInit(parameters: parameters)
        
    }
    
    
    func requestInit(parameters: Parameters) -> Void {
        
        Alamofire.request(Api.URL + "auth/init", method:.post, parameters: parameters, encoding: JSONEncoding.default, headers:Api.getHeaders()).responseJSON { response in
            switch response.result {
            case .success:
                debugPrint(response)
                
                if let result = response.result.value {
                    
                    let JSON = result as! NSDictionary
                    if (JSON.object(forKey: "status") as! Bool){
                        
                        let responseJson = JSON.object(forKey: "response") as! NSDictionary
                        
                        let sessionToken:String = responseJson.object(forKey: "session_token") as! String
                        Preferences.getInstans().setSessionToken(sessionToken: sessionToken)
                        
                        let sessionTimeLeft:Int = responseJson.object(forKey: "session_time_left") as! Int
                        Preferences.getInstans().setSessionTimeLeft(sessionTimeLeft: sessionTimeLeft)
                        
                        let index = parameters.index(forKey: "cell_phone")
                        Preferences.getInstans().setPhoneNumber(phoneNumber: parameters[index!].value as! String)
                        
                        Api.updateSession()
                        SPAlert.present(message: "Приложение стартует")
                        
                        self.redirectToIncident()
        
                    }else{
                        
                        if (JSON.object(forKey: "error") as? Int == Api.ErrorCode.errorNeedAutorization){
                            
                            self.requestRegister(parameters: parameters)
                            SPAlert.present(message: "Введите код")
                            
                        }else {
                            SPAlert.present(message: "Не обработанная ошибка")
                        }
                        
                        if (JSON.object(forKey: "error") as? Int == Api.ErrorCode.errorInternal){
                            SPAlert.present(message: "Сервис временно недоступен. Попробуйте позже")
                        }    
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func requestRegister(parameters: Parameters) -> Void {
           Alamofire.request(Api.URL + "auth/register", method:.post, parameters: parameters, encoding: JSONEncoding.default, headers:Api.getHeaders()).responseJSON { response in
               switch response.result {
               case .success:
                   if let result = response.result.value {
                       let JSON = result as! NSDictionary
                       if (JSON.object(forKey: "status") as! Bool){
                           self.requestInit(parameters: parameters)
                       }else{
                           if (JSON.object(forKey: "error") as? Int == Api.ErrorCode.errorPhoneNotFound){
                            SPAlert.present(message: "Телефон не обслуживается")
                           }
                           
                           if (JSON.object(forKey: "error") as? Int == Api.ErrorCode.errorAuthorizationKeyDoNotSet ||
                               JSON.object(forKey: "error") as? Int == Api.ErrorCode.errorNeedAutorization){
                               self.showKeyDialog(oldParameters: parameters)
                           }
                        
                           if (JSON.object(forKey: "error") as? Int == Api.ErrorCode.errorKeyWrong){
                            SPAlert.present(message: "Неверный код")
                           }
                        
                            if (JSON.object(forKey: "error") as? Int == Api.ErrorCode.errorInternal){
                                SPAlert.present(message: "Сервис временно недоступен. Попробуйте позже")
                            }
                       }
                   }
               case .failure(let error):
                   print(error)
               }
           }
       }
    
    func showKeyDialog(oldParameters: Parameters) -> Void {
        showInputDialog(title: "Введите код регистрации",
                        subtitle: "Код отправлен на ваш номер.",
                        actionTitle: "ОК",
                        cancelTitle: "Отмена",
                        inputPlaceholder: "Код",
                        inputKeyboardType: .numberPad)
        { (input:String?) in
            if(input!.isEmpty){
             SPAlert.present(message: "Введите код")
            }else{
                let index = oldParameters.index(forKey: "cell_phone")
                let parameters: Parameters = [
                    "cell_phone": oldParameters[index!].value,
                    "key": input!
                ]
                self.requestRegister(parameters: parameters)
            }
        }
    }
    
    
     func redirectToIncident(){
        DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
            let menuTabController = self.storyboard?.instantiateViewController(withIdentifier: "tabBar") as! TabBarController
                menuTabController.selectedIndex = 2;
                       self.present(menuTabController, animated: false, completion: nil)
                   })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension UIViewController {
    func showInputDialog(title:String? = nil,
                         subtitle:String? = nil,
                         actionTitle:String? = "Add",
                         cancelTitle:String? = "Cancel",
                         inputPlaceholder:String? = nil,
                         inputKeyboardType:UIKeyboardType = UIKeyboardType.default,
//                         inputType:UITextField = UITextContentType.oneTimeCode,
                         cancelHandler: ((UIAlertAction) -> Swift.Void)? = nil,
                         actionHandler: ((_ text: String?) -> Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: subtitle, preferredStyle: .alert)
        alert.addTextField { (textField:UITextField) in
            textField.placeholder = inputPlaceholder
            textField.keyboardType = inputKeyboardType
            if #available(iOS 11.0, *) {} else {
                textField.textContentType = UITextContentType.oneTimeCode
            }
        }
        alert.addAction(UIAlertAction(title: actionTitle, style: .destructive, handler: { (action:UIAlertAction) in
            guard let textField =  alert.textFields?.first else {
                actionHandler?(nil)
                return
            }
            actionHandler?(textField.text)
        }))
        alert.addAction(UIAlertAction(title: cancelTitle, style: .cancel, handler: cancelHandler))
        
        self.present(alert, animated: true, completion: nil)
    }
}
