//
//  EquipmentListController.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 06/11/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit
import Foundation
import SPStorkController

class EquipmentListController: Controller{
    
    let collectionView = EquipmentCollectionView()
    let height: CGFloat = 400
    
    var equipments: [Equipment] = []
    var amountEquipment: [Int] = []
    
    func setEquipments(equipments: [Equipment]){
        self.equipments = equipments
    }
    
    weak var delegate: StorageDelegate?
    
    override func viewDidLoad() {
        self.view.addSubview(self.collectionView)
        super.viewDidLoad()
        self.view.backgroundColor = Color.Assest.bakground
        self.setNavigationTitle(Text.equipment, style: .small)
        
        if #available(iOS 11.0, *) {
            self.collectionView.contentInsetAdjustmentBehavior = .always
        }
        
        self.collectionView.contentInset.top = Space.vertical / 2
        self.collectionView.alwaysBounceVertical = true
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.showsVerticalScrollIndicator = true
        
        self.emptyTitlesView.titleLabel.text = Text.empty
        self.emptyTitlesView.subtitleLabel.text = Text.no_equipments
        
        if self.equipments.isEmpty {
            self.emptyTitlesView.isHidden = false
        }
        
        self.updateLayout(with: self.view.frame.size)
        
    }
    
    override func updateLayout(with size: CGSize) {
        super.updateLayout(with: size)
        self.collectionView.frame = CGRect.init(origin: .zero, size: size)
        self.collectionView.layout.maxWidth = size.width - (Space.side * 1.5)
    }
    
    override func storageUpdated() {
        self.collectionView.reloadData()
        self.emptyTitlesView.isHidden = !self.equipments.isEmpty
        SPStorkController.updatePresentingController(parent: self)
        self.delegate?.storageUpdated()
    }
}

extension EquipmentListController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.equipments.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueCell(indexPath: indexPath)
        let equipment = self.equipments[indexPath.row]
        cell.equipment = equipment
        cell.equipmentView.userNameLabel.text = equipment.equipment_title
        cell.equipmentView.commentLabel.text = equipment.equipment_uin
        cell.equipmentView.placeLabel.text = equipment.equipment_markplace
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delay(0.12, closure: {
//            let controller = IncidentController()
//            controller.setEquipment(equipment: self.equipments[indexPath.row])
//            self.delegate?.storageUpdated()
//            self.present(controller)
        })
    }
}

protocol EquipmentListDelegate: class {
    
    func updatedIncident( _ incident: Incident?)
}

