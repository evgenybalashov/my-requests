//
//  SPFirebaseAnalytics.swift
//  applicantsOffice
//
//  Created by Евгений Балашов on 23/10/2019.
//  Copyright © 2019 example. All rights reserved.
//

import UIKit
import Firebase

import Foundation

extension SPFirebase {
    
    enum Analytics {}
}

extension SPFirebase.Analytics {
    
    static func event(_ id: String, parameters: [String : String]) {
        Analytics.logEvent(id, parameters: parameters)
    }
    
    static func setUserProperty(_ id: String, value: String) {
        Analytics.setUserProperty(value, forName: id)
    }
}
